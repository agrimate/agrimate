module TestPreprocess

using Test
using Dates

include("../src/preprocess.jl")


@test date_to_fractional_year(Date("2020-01-01")) - 2020 ==  2021 - date_to_fractional_year(Date("2020-12-31"))

@test date_to_timestep(Date("2020-06-30"); start=Date("2020-01-01"), N_year = 365) == Dates.dayofyear(Date(2001,6,30))
@test date_to_timestep(Date("2020-06-30"); start=Date("2020-01-02"), N_year = 365) == Dates.dayofyear(Date(2001,6,30)) - 1
@test date_to_timestep(Date("2020-12-31"); start=Date("2020-01-01"), N_year = 365) == Dates.dayofyear(Date(2001,12,31))

@test date_to_timestep(Date("2020-06-30"); start=Date("2020-01-01"), N_year = 48) == 24

@test timestep_to_fractional_year(1; start=Date("2020-01-01"), N_year = 48) - 2020 ==
    2021 - timestep_to_fractional_year(48; start=Date("2020-01-01"), N_year = 48)


ms_to_days(ms) = Dates.value(ms) / (24 * 3600 * 1000)

dt1 = timestep_to_datetime(1; start=Date("2021-01-01"), N_year = 48)
dt2 = timestep_to_datetime(2; start=Date("2021-01-01"), N_year = 48)
dt48 = timestep_to_datetime(48; start=Date("2021-01-01"), N_year = 48)

@test ms_to_days((dt1 - DateTime(2021, 1, 1, 0, 0, 0))) == 0.5 * 365 / 48
@test ms_to_days(dt2 - dt1) == 365 / 48
@test ms_to_days(DateTime(2022, 1, 1, 0, 0, 0) - dt48) == 0.5 * 365 / 48

# Leap year

dt1 = timestep_to_datetime(1; start=Date("2020-01-01"), N_year = 48)
dt2 = timestep_to_datetime(2; start=Date("2020-01-01"), N_year = 48)
dt48 = timestep_to_datetime(48; start=Date("2020-01-01"), N_year = 48)

@test ms_to_days((dt1 - DateTime(2020, 1, 1, 0, 0, 0))) == 0.5 * 366 / 48
@test ms_to_days(dt2 - dt1) == 366 / 48
@test ms_to_days(DateTime(2021, 1, 1, 0, 0, 0) - dt48) == 0.5 * 366 / 48

end