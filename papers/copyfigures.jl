using DrWatson
@quickactivate "Agrimate"

mkpath(papersdir("model_paper", "figures"))

figures = Dict(
    "wm_price;years=2006-2012.pdf" => "wm_price.pdf",
    "producer;region=Russia;years=2006-2012.pdf" => "producer;region=Russia.pdf",
    "consumer;region=Egypt;years=2006-2012.pdf" => "consumer;region=Egypt.pdf",
    "partners;consumer=Egypt;producer=Russia;years=2006-2012.pdf" => "partners;consumer=Egypt;producer=Russia.pdf",
    "comparison;years=2007.png" => "comparison;years=2007.png",
    "anomalies;years=2007-2008.pdf" => "anomalies;years=2007-2008.pdf",
)

for (src, dst) in figures
    cp(
        plotsdir(
            "agrimate_plots",
            "food_crisis",
            "paper",
            "paper;crop=wheat;extra_regions=(Egypt=EGY);start=2004-01-01",
            src,
        ),
        papersdir("model_paper", "figures", dst);
        force = true,
    )
end

cp(
    plotsdir("geoplots", "regions.png"),
    papersdir("model_paper", "figures", "regions.png");
    force = true,
)