# Agrimate

## Repository description

Agrimate is a global dynamic agent-based network model of sub-annual price and trade dynamics in agricultural
markets developed at [Potsdam Institute for Climate Impact Research](https://www.pik-potsdam.de/). This repository accompanies the paper: "Modeling sub-annual price and trade flow anomalies in agricultural markets: the dynamic agent-based model: Agrimate" authored by Patryk Kubiczek, Kilian Kuhla, and Christian Otto.

The repository is composed of the following parts:
* Julia code base for the Agrimate model in the `src/AgrimateModel` directory
* Julia functions used to run the model and process the output data in the `src` directory
* Python code base for the processing of input data in the `src/python/AgriculturalData` directory
* Python and Julia scripts used to generate the actual input data, run the model and produce paper results in the `scripts` directory
* input and output data in the `data` directory
* figures representing input and output data in the `plots` directory
* paper figures in the `papers` directory

## Reproducing the results

### Python

Parts of the code responsible for the download, processing, and analysis of the input data are written in Python. 

This project is using [Poetry](https://python-poetry.org) tool to manage the Python dependencies. The dependencies are listed in the `pyproject.toml` file. To install the dependencies, run the following command in the project directory: 
```
poetry install
```

It might be necessary to add the `src/python` directory to the `PYTHONPATH` environment variable. To do so, run the following commands (here we assume you use Python version 3.8 and the Bash shell):
```
poetry shell
pushd src/python/
pwd >> $VIRTUAL_ENV/lib/python3.8/site-packages/AgriculturalData.pth
popd
```
Before executing any Python scripts, make sure to activate the virtual environment by running
```
poetry shell
``` 
in the project directory.

To generate all the input needed to run the model, run the following command in the project directory:
```
python scripts/python/make_agrimate_input.py
```

The script `make_agrimate_input.py` lists all the steps and corresponding scripts needed to generate the input data. If any problems occur, or if you want to generate only a subset of the input data, you can run the individual scripts listed in the `make_agrimate_input.py` file. In principle, the generation of input data can be fully skipped as all the relevant input data are already included in the `data` directory.

### Julia

The Agrimate model itself, and the postprocessing of the model output, are written in Julia.

This project is using [DrWatson](https://juliadynamics.github.io/DrWatson.jl/stable/) package to manage the reproduction of the results. 
To locally reproduce this project, open a Julia console in the project directory and run the following commands:
```
using Pkg
Pkg.activate(".")
Pkg.instantiate()
```
This will install all the necessary packages for you to be able to run the scripts.

To run the model for the scenarios described in the paper, run the following command in the project directory:
```
julia scripts/runs/food_crisis.jl
```
This will run the model for the food crisis scenario and save the results in the `data/agrimate_output` directory. 

If you want to make use of MPI parallelization, prepend the command with the corresponding MPI command and the desired number of cores, e.g.
```
mpirun -n 4 julia scripts/runs/food_crisis.jl
```

To generate the figures based on the model output, run the following command:
```
julia scripts/visualizations/food_crisis_viz.jl
```
This will generate the figures in the `plots/agrimate_plots` directory.

If you want to experiment with the model, use the `scripts/runs/default.jl` script as a template. All the input parameters of the model (together with their default values) are listed in `src/AgrimateModel/src/AgrimateModel.jl` and `src/params.jl`.






