using DrWatson
@quickactivate "Agrimate"

include(srcdir("postprocess.jl"))
include(srcdir("plot.jl"))


files = [
    datadir(
        "agrimate_output", 
        savename(
            Params(
                τ_P=1000.
            ), 
            "csv"; 
            savename_kwargs...
        )
    )
]
df = get_combined_data(files)
axwidth = 400

for producer in unique(df[.!ismissing.(df.producer), :producer])
    @show producer
    fig = plot_agent(df; producer, axwidth)
    wsave(
        plotsdir(
            "agrimate_plots",
            "default", 
            savename("default", (;producer), "png"; savename_kwargs...),
        ),
        fig; 
        px_per_unit = 2,
    )
    (producer == "World") && continue
    fig = plot_partners(df; producer, axwidth)
    wsave(
        plotsdir(
            "agrimate_plots",
            "default",
            "partners", 
            savename("default", (;producer), "png"; savename_kwargs...),
        ),
        fig; 
        px_per_unit = 2,
    )
end
for consumer in unique(df[.!ismissing.(df.consumer), :consumer])
    @show consumer
    fig = plot_agent(df; consumer, axwidth)
    wsave(
        plotsdir(
            "agrimate_plots",
            "default", 
            savename("default", (;consumer), "png"; savename_kwargs...),
        ),            
        fig; 
        px_per_unit = 2,
    )
    (consumer == "World") && continue
    fig = plot_partners(df; consumer, axwidth)
    wsave(
        plotsdir(
            "agrimate_plots",
            "default",
            "partners", 
            savename("default", (;consumer), "png"; savename_kwargs...),
        ),
        fig; 
        px_per_unit = 2,
    )
end












