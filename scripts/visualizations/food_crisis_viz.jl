using DrWatson
@quickactivate "Agrimate"

using MPI
!MPI.Initialized() && MPI.Init()

include(srcdir("regions.jl"))
include(srcdir("preprocess.jl"))
include(srcdir("postprocess.jl"))
include(srcdir("plot.jl"))
include(srcdir("plot_geo.jl"))

# Scenario

viz_name = "food_crisis"

scenario_params = (;
    crops = "wheat",
    baseline = "2004-2006",
    start = Date("2004-01-01"),
    regions = "AgrimateEU28",
    # extra_regions = Dict("Kenya" => "KEN")
    extra_regions = Dict("Egypt" => "EGY"),
    λ = 1.0,
    ψ = :empirical,
    A_d_star = :empirical,
    A_c_star = :empirical,
    production_anomalies = "FAO",
    export_restrictions = "2007-2011",
    production_cutoff = 0,
)
default_scenario_params = Params(; scenario_params...)

# Parameters to explore

param_space = Dict(
    :crops => [
        "wheat",
        # "rice",
        # "maize",
    ],
    :start => [
        Date("2004-01-01"),
        # Date("2007-01-01"),
    ],
    :extra_regions => [
        Dict("Egypt" => "EGY"),
        # Dict("Kenya" => "KEN"),
    ],
    :baseline => [
        @onlyif(:start == Date("2004-01-01"), "2004-2006"),
        # @onlyif(:start == Date("2007-01-01"), "2007-2009"),
    ],
    :export_restrictions => [nothing, "2007-2011"],
)

extra_param_space = Dict(
    :production_anomalies => ["FAO", "USDA"],
    :N_year => [24, 36, 48],
    :N_del => ([1, 2, 3] .* (default_scenario_params.N_year ÷ 12)),
    :N_hor => [1, 2, 3] .* default_scenario_params.N_year,
    :N_for => [0, 3, 6] .* (default_scenario_params.N_year ÷ 12),
    :α => [4.0, 5.0, 6.0],
    :β => [0.01, 0.05, 0.10],
    :p_sto => [0.05, 0.1, 0.15],
    :σ => [1.5, 2.0, 2.5],
    :τ_a => [0.05, 0.2, 0.5],
    :τ_P => [0.1, 0.2, 0.5],
    :τ_exp => [0.2, 0.5, 1.0],
    :τ_for => [0.1, 0.2, 0.5],
    :τ => [0.1, 0.2, 0.5],
    :ι => [0.001, 0.005, 0.01],
    :ε_c => [0.05, 0.1, 0.2],
    :ψ => [Symbol("empirical_EGY=$v") for v in [0.1, 0.5, 1.0]],
    :A_d_star => [Symbol("empirical_EGY=$v") for v in [0.01, 0.2, 0.8]],
    :A_c_star => [Symbol("empirical_EGY=$v") for v in [0.01, 0.2, 0.8]],
    :flow_cutoff => [0.005, 0.01, 0.02],
)

param_labels = Dict{Symbol,String}(
    :export_restrictions => "export restrictions",
    :production_anomalies => "production anomalies",
    :A_d_star => "A_d*",
    :A_c_star => "A_c*",
)
for key in keys(extra_param_space)
    key in keys(param_labels) && continue
    param_labels[key] = string(key)
end

value_labels = Dict{Symbol,Any}(
    :export_restrictions => Dict(nothing => "no", "2007-2011" => "yes"),
    (
        param => Dict(
            v => begin
                (area, val) = split(split(string(v), "_")[end], "=")
                "$val ($area)"
            end for v in extra_param_space[param]
        ) for param in [:ψ, :A_d_star, :A_c_star]
    )...,
)
for key in keys(extra_param_space)
    key in keys(value_labels) && continue
    value_labels[key] = Dict(v => v for v in extra_param_space[key])
end

function plot_everything(; extra_params = nothing, plot_geoplots = true)
    comm = MPI.COMM_WORLD
    n_workers = MPI.Comm_size(comm)
    worker_id = MPI.Comm_rank(comm) + 1

    if worker_id == 1
        @info "Number of workers" n_workers
        flush(stderr)
    end

    tasks = [
        (; crop, start, extra_regions, param, param_values) for
        (param, param_values) in extra_param_space for
        (crop, start, extra_regions) in Iterators.product(
            param_space[:crops],
            param_space[:start],
            param_space[:extra_regions],
        ) if isnothing(extra_params) || param in extra_params
    ]

    n_tasks = length(tasks)
    if worker_id == 1
        @info "Number of tasks" n_tasks
        flush(stderr)
    end
    worker_task_ids = [collect(i:n_workers:n_tasks) for i in 1:n_workers]
    worker_tasks = tasks[worker_task_ids[worker_id]]

    for task in worker_tasks
        @info "Worker id: $worker_id, number of tasks: $(length(worker_tasks))"
        flush(stderr)

        @unpack crop, start, extra_regions, param, param_values = task

        theme = set_my_theme!()

        function format_extra_regions(extra_regions)
            return "(" *
                   join(("$region=$areas" for (region, areas) in extra_regions), ";") *
                   ")"
        end

        function viz_plotsdir(path...)
            return plotsdir(
                "agrimate_plots",
                viz_name,
                savename(
                    viz_name,
                    (;
                        crop,
                        start,
                        extra_regions = format_extra_regions(extra_regions),
                        param,
                    );
                    savename_kwargs...,
                ),
                path...,
            )
        end

        function produceplot(f, subdir, prefix, config = (;); suffix = "pdf", force = true)
            return produce_or_load(
                x -> (flush(stderr); f(x)),
                viz_plotsdir(subdir),
                config;
                prefix,
                suffix,
                wsave_kwargs = suffix == "png" ? (; px_per_inch = 300) :
                               (; pt_per_unit = 1),
                loadfile = false,
                force,
                savename_kwargs...,
            )
        end

        param_dicts = dict_list(param_space)
        param_dicts = [
            d for d in param_dicts if
            # Fixed parameters
            (d[:crops] == crop) &&
            (d[:start] == start) &&
            (d[:extra_regions] == extra_regions)
        ]
        param_dicts = [
            merge(param_dict, Dict(param => value)) for value in param_values for
            param_dict in param_dicts
        ]

        make_params(param_dict) = begin
            params = merge(scenario_params, (; param_dict...))
            params = Params(; params...)
            return params
        end

        files = [
            datadir(
                "agrimate_output",
                savename(make_params(param_dict), "csv"; savename_kwargs...),
            ) for param_dict in param_dicts
        ]

        labels = [
            Dict(
                param_labels[p] => value_labels[p][value] for
                (p, value) in param_dict if p in [:export_restrictions, param]
            ) for param_dict in param_dicts
        ]
        df = get_combined_data(files, labels)
        skip_years = (2, 1)

        df = define_runs!(df)

        runs = ["baseline", "prod. anom.", "prod. anom. + exp. rest."]
        zorderdict = Dict(r => i for (i, r) in enumerate(reverse(runs)))
        colordict = Dict(runs .=> theme.palette.color[][3:-1:1])
        linestyledict = Dict(runs .=> theme.palette.linestyle[][3:-1:1])
        param = param_labels[param]

        df_region = make_regions_dataframe(
            get_regions(scenario_params.regions),
            scenario_params.extra_regions,
        )
        df_emp = process_empirical_data(crop, df_region)
        df_sim = process_simulation_data(df; param)

        variables = [
            "Production",
            "Purchase",
            "Consumption",
            "Stock Variation",
            "Consumption + Stock Var.",
        ]
        cols = vcat("Region", "Year", "Label", "Anomaly " .* variables)
        df_emp = select(df_emp, cols)
        df_sim = select(df_sim, vcat(cols, param))

        # Change mass units to mln tonnes
        df[is_extensive.(df.variable), :value] ./= 1000
        # Change flow units to mln tonnes / month
        isflow = is_extensive.(df.variable) .& .!endswith.(df.variable, Ref("storage"))
        if :N_year in propertynames(df)
            df[isflow, :value] .*= df[isflow, :N_year] / 12
        else
            df[isflow, :value] .*= default_scenario_params.N_year / 12
        end

        # Monthly price plot
        produceplot("monthly", viz_name, (; plot = "wm_price"); force = true) do config
            fig, df_price = plot_wm_price_timeseries(
                df,
                crop;
                zorderdict,
                colordict,
                linestyledict,
                runs,
                param,
                skip_years,
                price_ref_run = "prod. anom. + exp. rest.",
                default_scenario_params.σ,
            )
            return fig
        end

        # Annual anomaly plots
        for (key, subdf_sim) in pairs(groupby(df_sim, :Region))
            region = key[:Region]
            (param ∈ ["ψ", "A_d*", "A_c*"] && region != "Egypt") && continue

            produceplot("annual", viz_name, (; region); force = true) do config
                labels = [
                    "sim. prod. anom.",
                    "sim. prod. anom. + exp. rest.",
                    "emp. FAO",
                    "emp. USDA",
                ]
                palette = theme.palette.color[]
                labelcolordict = Dict(labels .=> palette[[2, 1, 4, 5]])
                fig = plot_aggregates(
                    subdf_sim,
                    df_emp;
                    labels,
                    colordict = labelcolordict,
                    param,
                    region,
                    skip_years,
                    linkyaxes = :minimal,
                    barplot = false,
                    ylabel = "Anomaly",
                )
                return fig
            end
        end

        # Geoplots **************************

        if plot_geoplots
            year_sels = Dict(
                Date("2004-01-01") => [(2007,), (2008,), (2007, 2008)],
                Date("2007-01-01") => [(2010,), (2011,), (2010, 2011)],
            )

            for year_sel in year_sels[start]
                year_string =
                    length(year_sel) == 1 ? "$(year_sel[1])" :
                    "$(year_sel[begin])-$(year_sel[end])"

                dfs_geo = aggregate_anomalies.([df_sim, df_emp], Ref(year_sel); param)
                df_param = DataFrame(param => unique(dfs_geo[1][!, param]))
                dfs_geo[2] = crossjoin(dfs_geo[2], df_param)
                df_geo = vcat(dfs_geo...; cols = :union)
                df_geo = stack(df_geo, Not([:Region, :Label, Symbol(param)]))

                # Comparison plots
                for (key, subdf_geo) in pairs(groupby(df_geo, param))
                    produceplot(
                        "geoplots",
                        viz_name,
                        (;
                            years = year_string,
                            plot = "comparison",
                            (Symbol(param) => key[param],)...,
                        );
                        suffix = "png",
                        force = true,
                    ) do config
                        fig = plot_geolayout(
                            make_gdf(; df_region, df = subdf_geo[!, Not(param)]);
                            row_col = :variable,
                            column_col = :Label,
                            unit = "Anomaly $year_string",
                            limits = (-0.15, 0.15),
                        )
                        return fig
                    end
                end

                # Role of export restrictions plot
                produceplot(
                    "geoplots",
                    viz_name,
                    (; years = year_string, plot = "Δ_export_restrictions");
                    suffix = "png",
                    force = true,
                ) do config
                    df_Δ = unstack(df_geo, :Label, :value)
                    df_Δ = transform(
                        df_Δ,
                        ["sim. prod. anom. + exp. rest.", "sim. prod. anom."] =>
                            ByRow((v, v0) -> (v - v0) / (1 + v0)) => "Δ exp. rest.",
                    )
                    df_Δ = stack(
                        df_Δ,
                        ["Δ exp. rest."],
                        ["Region", param, "variable"];
                        variable_name = :Label,
                    )[
                        :,
                        Not(:Label),
                    ]

                    gdf = make_gdf(; df_region, df = df_Δ)
                    transform!(gdf, param => ByRow(v -> "$param = $v") => param)
                    fig = plot_geolayout(
                        gdf;
                        row_col = :variable,
                        column_col = param,
                        unit = "Relative Difference $year_string",
                        limits = (-0.15, 0.15),
                        colormap = :diverging_gwv_55_95_c39_n256,
                        rev = true,
                    )
                    return fig
                end

                # Empirical plot 
                produceplot(
                    "geoplots",
                    viz_name,
                    (; plot = "empirical", years = year_string);
                    suffix = "png",
                    force = true,
                ) do config
                    df_geo_emp = subset(df_geo, :Label => ByRow(x -> startswith(x, "emp")))
                    df_geo_emp[!, :Label] .*= "\n(by region)"
                    df_geo_emp = select(df_geo_emp, Not(param)) |> unique
                    df_geo_emp = subset(df_geo_emp, :variable => ByRow(!=("Purchase")))
                    gdf = make_gdf(; df_region, df = df_geo_emp)
                    fig = plot_geolayout(
                        gdf;
                        row_col = :variable,
                        column_col = :Label,
                        unit = "Anomaly $year_string",
                    )

                    df_country = copy(df_region)
                    df_country[findfirst(==("GBR"), df_country.Area), :Region] = "EU-28"
                    df_country[df_country.Region .== "EU-27", :Region] .= "EU-28"
                    is_not_eu = df_country.Region .!= "EU-28"
                    df_country[is_not_eu, :Region] = df_country[is_not_eu, :Area]
                    df_geo_emp_countries = aggregate_anomalies(
                        process_empirical_data(crop, df_country),
                        year_sel,
                    )
                    df_geo_emp_countries =
                        stack(df_geo_emp_countries, Not([:Region, :Label]))
                    df_geo_emp_countries[!, :Label] .*= "\n(by country)"
                    df_geo_emp_countries =
                        subset(df_geo_emp_countries, :variable => ByRow(!=("Purchase")))
                    gdf = make_gdf(; df_region = df_country, df = df_geo_emp_countries)
                    fig = plot_geolayout(
                        gdf;
                        row_col = :variable,
                        column_col = :Label,
                        unit = "Anomaly $year_string",
                        fig,
                        i0 = 0,
                        j0 = 3,
                        drawrowlabels = false,
                    )
                    legends = contents(fig[end, :])
                    delete!(legends[2])
                    fig[end, :] = legends[1]
                    resize_to_layout!(fig)
                    return fig
                end
            end
        end
        # ***********************************

        # Agent and partner plots

        for (key, subdf) in pairs(groupby(df, :producer))
            @unpack producer = key
            ismissing(producer) && continue
            (param ∈ ["ψ", "A_d*", "A_c*"] && producer != "Egypt") && continue

            produceplot("agent", viz_name, (; producer); force = true) do config
                sel_variables = [
                    "harvest" => "harvest (mln t / month)",
                    "export restriction" => "export restriction magnitude",
                    "export tax factor" => "export tax factor",
                    "incoming demand" => "incoming demand (mln t / month)",
                    "sales" => "sales (mln t / month)",
                    "producer storage" => "producer storage (mln t)",
                    "price adjustment factor" => "price adjustment factor",
                    "selling price" => "selling price index",
                ]
                fig = plot_agent(
                    subdf;
                    producer,
                    sel_variables,
                    param,
                    zorderdict,
                    colordict,
                    linestyledict,
                    runs,
                    skip_years,
                    default_scenario_params.σ,
                )
                return fig
            end

            (producer == "World") && continue
            produceplot("partners", viz_name, (; producer); force = true) do config
                fig = plot_partners(
                    subdf;
                    producer,
                    param,
                    zorderdict,
                    colordict,
                    linestyledict,
                    runs,
                    skip_years,
                )
                return fig
            end
        end

        for (key, subdf) in pairs(groupby(df, :consumer))
            @unpack consumer = key
            ismissing(consumer) && continue
            (param ∈ ["ψ", "A_d*", "A_c*"] && consumer != "Egypt") && continue

            produceplot("agent", viz_name, (; consumer); force = true) do config
                sel_variables = [
                    "demand" => "demand (mln t / month)",
                    "extra demand" => "extra demand (mln t / month)",
                    "purchase" => "purchase (mln t / month)",
                    "consumption" => "consumption (mln t / month)",
                    "consumer storage" => "consumer storage (mln t)",
                    "consumer price" => "consumer price index",
                ]
                fig = plot_agent(
                    subdf;
                    consumer,
                    sel_variables,
                    param,
                    zorderdict,
                    colordict,
                    linestyledict,
                    runs,
                    skip_years,
                )
                return fig
            end

            (consumer == "World") && continue
            produceplot("partners", viz_name, (; consumer); force = true) do config
                fig = plot_partners(
                    subdf;
                    consumer,
                    param,
                    zorderdict,
                    colordict,
                    linestyledict,
                    runs,
                    skip_years,
                )
                return fig
            end
        end
    end
end

function plot_paper()
    theme = set_my_theme!()
    titlefont = theme.Axis.titlefont
    legendfont = theme.Legend.labelfont
    titlesize = theme.fontsize[] + 1

    for (crops, start, extra_regions) in Iterators.product(
        param_space[:crops],
        param_space[:start],
        param_space[:extra_regions],
    )
        @show crops, start, extra_regions
        flush(stdout)

        function format_extra_regions(extra_regions)
            return "(" *
                   join(("$region=$areas" for (region, areas) in extra_regions), ";") *
                   ")"
        end

        function paper_plotsdir(path...)
            return plotsdir(
                "agrimate_plots",
                viz_name,
                "paper",
                savename(
                    "paper",
                    (;
                        crop = crops,
                        start,
                        extra_regions = format_extra_regions(extra_regions),
                    );
                    savename_kwargs...,
                ),
                path...,
            )
        end

        function produceplot(f, prefix, config = (;); suffix = "pdf", force = true)
            return produce_or_load(
                f,
                paper_plotsdir(),
                config;
                prefix,
                suffix,
                wsave_kwargs = suffix == "png" ? (; px_per_inch = 300) :
                               (; pt_per_unit = 1),
                loadfile = false,
                force,
                savename_kwargs...,
            )
        end

        param_dicts = dict_list(param_space)
        param_dicts = [
            d for d in param_dicts if
            # Fixed params 
            (d[:crops] == crops) &&
            (d[:start] == start) &&
            (d[:extra_regions] == extra_regions)
        ]

        make_params(param_dict) = begin
            params = merge(scenario_params, (; param_dict...))
            params = Params(; params...)
            return params
        end

        files = [
            datadir(
                "agrimate_output",
                savename(make_params(param_dict), "csv"; savename_kwargs...),
            ) for param_dict in param_dicts
        ]

        labels = [
            Dict(
                param_labels[param] => value_labels[param][value] for
                (param, value) in param_dict if param == :export_restrictions
            ) for param_dict in param_dicts
        ]
        df = get_combined_data(files, labels)

        runs = [
            "baseline",
            "production anomalies",
            "prod. anom. + export restrictions",
        ]
        df = define_runs!(df; runs)
        zorderdict = Dict(r => i for (i, r) in enumerate(reverse(runs)))
        colordict = Dict(r => c for (r, c) in zip(runs, theme.palette.color[][3:-1:1]))
        linestyledict =
            Dict(r => ls for (r, ls) in zip(runs, theme.palette.linestyle[][3:-1:1]))

        df_region = make_regions_dataframe(
            get_regions(scenario_params.regions),
            scenario_params.extra_regions,
        )
        df_emp = process_empirical_data(crops, df_region)
        df_sim = process_simulation_data(df)

        # Change mass units to mln tonnes
        df[is_extensive.(df.variable), :value] ./= 1000
        # Change flow units to mln tonnes / month
        isflow = is_extensive.(df.variable) .& .!endswith.(df.variable, Ref("storage"))
        if :N_year in propertynames(df)
            df[isflow, :value] .*= df[isflow, :N_year] / 12
        else
            df[isflow, :value] .*= default_scenario_params.N_year / 12
        end

        extra_consumer = first(keys(extra_regions))


        all_runs = runs
        for nrun in 1:length(all_runs), years in [(2006, 2010), (2006, 2012)]

            skip_years = (years[1] - 2004, 2013 - years[2])
            years = "$(years[1])-$(years[2])"
            runs = all_runs[1:nrun]
            suffix = nrun < length(all_runs) ? "_$nrun" : ""

            # WM price plot
            produceplot("wm_price" * suffix, (; years); force = true) do config
                fig, grid, df_plt = plot_wm_price_timeseries(
                    df,
                    crops;
                    skip_years,
                    price_ref_run = all_runs[end],
                    default_scenario_params.σ,
                    # rowlabelsposition = :left,
                    zorderdict,
                    colordict,
                    linestyledict,
                    runs,
                    # axwidth = 100,
                    # axheight = 75,
                    layout = true,
                )
                resize_to_layout!(fig)
                ylims!(grid[1,1].axis, (-5, 165))
                ylims!(grid[1,2].axis, (-16, 16))
                ylims!(grid[2,1].axis, (-0.05, 0.55))
                wsave(paper_plotsdir("data", "wm_price.csv"), df_plt)
                return fig
            end

            # Producer panel
            for region in ["Russia", "Ukraine"]
                produceplot("producer" * suffix, (; region, years); force = false) do config
                    producer = config.region

                    variables = [
                        "harvest" => "harvest (mln t / month)",
                        "incoming demand" => "incoming demand (mln t / month)",
                        "sales" => "sales (mln t / month)",
                        "export restriction" => "export restriction magnitude",
                        "producer storage" => "producer storage (mln t)",
                        "selling price" => "selling price index",
                    ]
                    fig, grid = plot_agent(
                        df;
                        runs,
                        producer,
                        skip_years,
                        sel_variables = variables,
                        title = "",
                        layout = true,
                        zorderdict,
                        colordict,
                        linestyledict,
                        returngrid = true,
                        default_scenario_params.σ,
                    )

                    # Set ylimits
                    if producer == "Ukraine" 
                        yrange = (-0.15, 3.5)
                        yrange_storage = (-1, 20)
                    end
                    if producer == "Russia"
                        yrange = (-0.15, 6.15)
                        yrange_storage = (-2.5, 50)
                    end

                    for loc in [(1, 2), (1, 3)]
                        ax = grid[loc...].axis
                        ylims!(ax, yrange)
                    end
                    ylims!(grid[2, 2].axis, yrange_storage)

                    yrange_price = (0.55, 1.45)
                    ylims!(grid[2, 3].axis, yrange_price)
                    yrange_er = (-0.05, 1.05)
                    ylims!(grid[2, 1].axis, yrange_er)

                    # Remove legend
                    leg = contents(fig[:, end])[1]
                    delete!(leg)
                    trim!(fig.layout)

                    # Add title
                    title = "Producer: $producer"
                    fig[0, :] = Label(fig, title; font = titlefont, textsize = titlesize)

                    # Resize and save
                    resize_to_layout!(fig)
                    return fig
                end
            end

            # Consumer panel
            # zorderdict = Dict(r => i for (i, r) in enumerate(reverse(all_runs)))


            produceplot("consumer" * suffix, (; region = extra_consumer, years); force = false) do config
                consumer = config.region

                variables = [
                    "demand" => "demand (mln t / month)",
                    "purchase" => "purchase (mln t / month)",
                    "consumption" => "consumption (mln t / month)",
                    "consumer storage" => "consumer storage (mln t)",
                    "consumer price" => "consumer price index",
                ]
                (fig, grid) = plot_agent(
                    transform(
                        df,
                        [:variable, :value] =>
                            ByRow((var, val) -> is_extensive(var) ? val * 1 : val) =>
                                :value,
                    );
                    runs,
                    zorderdict,
                    colordict,
                    linestyledict,
                    consumer,
                    skip_years,
                    sel_variables = variables,
                    title = "",
                    layout = true,
                    returngrid = true,
                )

                # Set ylimits
                # if consumer == "Kenya"
                #     yrange = (-5 / 1000, 105 / 1000)
                # end
                if consumer == "Egypt" 
                    yrange = (-0.15, 2.15)
                    yrange_storage = (0.5, 3.0)
                end
                

                for loc in [(1, 1), (1, 2), (1, 3)]
                    ax = grid[loc...].axis
                    ylims!(ax, yrange)
                end
                ylims!(grid[2, 1].axis, yrange_storage)

                yrange_price = (0.55, 1.45)
                ylims!(grid[2, 2].axis, yrange_price)

                # Move axes
                for (oldloc, newloc) in [(2, 2) => (2, 3), (2, 1) => (2, 2)]
                    (ax, label) =
                        (contents(fig[oldloc...])[], contents(fig[oldloc..., Top()])[])
                    (fig[newloc...], fig[newloc..., Top()]) = (ax, label)
                end
                # Fix xticklabels
                grid[1, 1].axis.xticklabelsvisible[] = true
                grid[1, 3].axis.xticklabelsvisible[] = false

                # Move legend
                leg = contents(fig[:, end])[]
                fig[2, 1] = leg
                trim!(fig.layout)

                # Add title
                title = "Consumer: $consumer"
                fig[0, :] = Label(fig, title; font = titlefont, textsize = titlesize)

                # Resize
                resize_to_layout!(fig)

                return fig
            end

            # Supplier plot
            for (producer, consumer) in
                [("Ukraine", extra_consumer), ("Russia", extra_consumer)]
                produceplot("partners" * suffix, (; producer, consumer, years); force = false) do config
                    tab20 = cgrad(:tab20, 20; categorical = true)
                    colordict_regions =
                        Dict(region_to_color(; extra_regions = extra_consumer => tab20[13])...)

                    fig = Figure()
                    _, grid = plot_partners(
                        df;
                        producer,
                        param = :run,
                        runs,
                        skip_years,
                        axwidth = 110,
                        axheight = 100,
                        colordict = colordict_regions,
                        fig = fig[1, 1],
                        returngrid = true,
                    )
                    producer == "Russia" && (yrange = (-0.2, 6.2))
                    producer == "Ukraine" && (yrange = (-0.1, 3.1))
                    for cell in grid
                        ax = cell.axis
                        ylims!(ax, yrange)
                    end
                    legend!(fig[1, 2], grid; rowgap = 1)
                    _, grid = plot_partners(
                        transform(
                            df,
                            [:variable, :value] =>
                                ByRow((var, val) -> is_extensive(var) ? val * 1 : val) =>
                                    :value,
                        );
                        consumer,
                        param = :run,
                        runs,
                        skip_years,
                        axwidth = 110,
                        axheight = 100,
                        colordict = colordict_regions,
                        unit = "mln t / month",
                        fig = fig[2, 1],
                        returngrid = true,
                    )
                    consumer == "Egypt" && (yrange = (-0.06, 2.06))
                    for cell in grid
                        ax = cell.axis
                        ylims!(ax, yrange)
                    end
                    legend!(fig[2, 2], grid; rowgap = 1)
                    resize_to_layout!(fig)
                    return fig
                end
            end
        end

        # Geoplot data

        variables = ["Production", "Consumption", "Stock Variation"]
        cols = vcat("Region", "Year", "Label", "Anomaly " .* variables)
        df_emp = select(df_emp, cols...)
        df_sim = select(df_sim, cols...)

        # Empirical production data by FAO is by definition equal the input data of simulation
        # It is wrong to keep the empirical production data as they are since they are aggregated assuming values correspond to calendar year which is NOT the case   
        allowmissing!(df_emp)
        allowmissing!(df_sim)
        production_anomalies_source = "FAO"
        df_emp_prod = unique(
            select(
                df_sim,
                "Region",
                "Year",
                "Anomaly Production" =>
                    "Anomaly Production " * production_anomalies_source,
            ),
        )
        df_emp = leftjoin(df_emp, df_emp_prod; on = ["Region", "Year"])
        empirical_from_source = df_emp.Label .== "emp. " * production_anomalies_source
        df_emp[empirical_from_source, "Anomaly Production"] .= df_emp[
            empirical_from_source,
            "Anomaly Production " * production_anomalies_source,
        ]
        df_emp[.!empirical_from_source, "Anomaly Production"] .= missing
        df_emp = select(df_emp, Not("Anomaly Production " * production_anomalies_source))

        # Geoplot: empirical data comparison

        year_sels = Dict(
            Date("2004-01-01") => [(2007,), (2008,), (2007, 2008)],
            Date("2007-01-01") => [(2010,), (2011,), (2010, 2011)],
        )

        for year_sel in year_sels[start]
            year_string =
                length(year_sel) == 1 ? "$(year_sel[1])" :
                "$(year_sel[begin])-$(year_sel[end])"

            dfs_geo = aggregate_anomalies.([df_sim, df_emp], Ref(year_sel))
            df_geo = vcat(dfs_geo...; cols = :union)
            df_geo = stack(df_geo, Not([:Region, :Label]))

            produceplot("anomalies", (; years = year_string); force = false) do config
                labels = [
                    "sim. production anomalies" => "production anomalies",
                    "sim. prod. anom. + export restrictions" => "prod. anom. + export restrictions",
                ]
                colordict = Dict(
                    l => c for (l, c) in zip(first.(labels), theme.palette.color[][2:-1:1])
                )
                fig = plot_anomalies(
                    df_geo;
                    regions = ["Russia", "Ukraine", "Argentina", "EU-28", extra_consumer],
                    labels,
                    variables = ["Consumption", "Stock Variation"],
                    colordict,
                    axwidth = 200,
                    axheight = 90,
                    ylabel = "Anomaly $year_string",
                )
                return fig
            end

            produceplot(
                "comparison",
                (; years = year_string);
                suffix = "png",
                force = false,
            ) do config
                labels = ["sim. prod. anom. + export restrictions", "emp. FAO"]
                select_labels = df -> subset(df, :Label => ByRow(in(labels)))

                palette = theme.palette.color[]
                colordict = Dict(labels[1] => palette[1], labels[2] => palette[4])

                fig = plot_aggregates(
                    df_sim |> select_labels,
                    df_emp |> select_labels;
                    region = "World",
                    labels,
                    colordict,
                    skip_years = (2, 1),
                    rowlabelsposition = :left,
                    legendposition = :bottom,
                    variables,
                    axwidth = 130,
                    axheight = 62,
                    marksource = false,
                    showbaseline = false,
                    title = "Global anomaly",
                )
                fig = plot_geolayout(
                    make_gdf(; df_region, df = df_geo |> select_labels);
                    fig,
                    i0 = 0,
                    j0 = 1,
                    row_col = :variable,
                    column_col = :Label,
                    unit = "Anomaly",
                    limits = (-0.15, 0.15),
                    drawrowlabels = false,
                    axwidth = 150,
                    axheight = 68,
                )
                delete!.(contents(fig[1, 2:3]))
                fig[1, 2:3] = Label(fig, "Regional anomaly $year_string"; font = titlefont)
                for n in 1:length(labels)
                    fig[4, 1 + n, Bottom()] =
                        Label(fig, labels[n]; padding = (0, 0, 0, 5), font = legendfont)
                end
                colgap!(fig.layout, 2)
                resize_to_layout!(fig)
                return fig
            end
        end

        # Geoplot: delta export restriction

        produceplot("delta_export_restrictions"; suffix = "png", force = false) do config
            year_one = first(year_sels[start])[1]
            year_two = year_one + 1
            dfs = []
            for year in (year_one, year_two)
                dfs_geo = aggregate_anomalies.([df_sim, df_emp], Ref((year,)))
                df_geo = vcat(dfs_geo...; cols = :union)
                df_geo = stack(df_geo, Not([:Region, :Label]))
                df_geo[!, :Year] .= year
                push!(dfs, df_geo)
            end

            df_geo = vcat(dfs...)

            variables = ["Consumption", "Stock Variation"]
            df_geo = subset(df_geo, :variable => ByRow(in(variables)))

            df_geo = unstack(df_geo, :Label, :value)
            df_geo = transform(
                df_geo,
                [
                    "sim. prod. anom. + export restrictions",
                    "sim. production anomalies",
                ] => ByRow((v, v0) -> (v - v0) / (1 + v0)) => "Δ exp. rest.",
            )
            df_geo = stack(
                df_geo,
                ["Δ exp. rest."],
                ["Region", "Year", "variable"];
                variable_name = :Label,
            )[
                :,
                Not(:Label),
            ]

            fig = plot_geolayout(
                make_gdf(; df_region, df = df_geo);
                row_col = :variable,
                column_col = :Year,
                unit = "Relative Difference",
                limits = (-0.15, 0.15),
                colormap = :diverging_gwv_55_95_c39_n256,
                rev = true,
            )
            return fig
        end

        continue
        for (extra_param, extra_param_values) in extra_param_space
            extra_param ∉ [:A_c_star, :A_d_star, :ψ] && continue

            param_dicts_ext = [
                merge(param_dict, Dict(extra_param => value)) for
                value in extra_param_values for param_dict in param_dicts
            ]
            files = [
                datadir(
                    "agrimate_output",
                    savename(make_params(param_dict), "csv"; savename_kwargs...),
                ) for param_dict in param_dicts_ext
            ]
            labels = [
                Dict(
                    param_labels[param] => value_labels[param][value] for
                    (param, value) in param_dict if
                    param in [:export_restrictions, extra_param]
                ) for param_dict in param_dicts_ext
            ]
            df = get_combined_data(files, labels)
            runs = [
                "baseline",
                "production anomalies",
                "prod. anom. + export restrictions",
            ]
            df = define_runs!(df; runs)
            zorderdict = Dict(r => i for (i, r) in enumerate(reverse(runs)))
            colordict = Dict(r => c for (r, c) in zip(runs, theme.palette.color[][3:-1:1]))
            linestyledict =
                Dict(r => ls for (r, ls) in zip(runs, theme.palette.linestyle[][3:-1:1]))

            # Change mass units to mln tonnes
            df[is_extensive.(df.variable), :value] ./= 1000
            # Change flow units to mln tonnes / month
            isflow = is_extensive.(df.variable) .& .!endswith.(df.variable, Ref("storage"))
            if :N_year in propertynames(df)
                df[isflow, :value] .*= df[isflow, :N_year] / 12
            else
                df[isflow, :value] .*= default_scenario_params.N_year / 12
            end

            produceplot(
                "sensitivity",
                (; region = extra_consumer, param = extra_param);
                force = true,
            ) do config
                @unpack region, param = config

                variables = [
                    "demand" => "demand (mln t / month)",
                    "consumption" => "consumption (mln t / month)",
                    "consumer price" => "consumer price index",
                ]
                fig = plot_agent(
                    df;
                    param = param_labels[param],
                    runs,
                    consumer = region,
                    colordict,
                    linestyledict,
                    zorderdict,
                    rowlabelsposition = :left,
                    skip_years,
                    sel_variables = variables,
                )
                return fig
            end
        end
    end
end

# plot_everything(; plot_geoplots = true)
plot_paper()