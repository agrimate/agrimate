using DrWatson
@quickactivate "Agrimate"

using Dates: Date

include(srcdir("params.jl"))
include(srcdir("io.jl"))

function renameoutput(outputdir = datadir("agrimate_output"); dry = true)
    for (root, dirs, files) in walkdir(outputdir)
        for file in files
            meta = wload(joinpath(root, file); metaonly = true)
            config = Dict(Symbol(k) => v for (k, v) in meta["params"])
            config[:start] = Date(config[:start])
            for param in [:rationing, :ψ, :A_d_star, :A_c_star]
                val = config[param]
                val isa String && (config[param] = Symbol(val))
            end
            params = Params(; config...)
            # (prefix, config, suffix) = parse_savename(file; savename_kwargs...)
            # delete!(config, param)
            newfile = savename("agrimate", params, "csv"; savename_kwargs...)
            if file != newfile
                @info join(["Renaming...", "Old filename: $file", "New filename: $newfile"], "\n")
            end
            if !dry
                mv(joinpath(root, file), joinpath(root, newfile))
            end
        end
    end
end

renameoutput()