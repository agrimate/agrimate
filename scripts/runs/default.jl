module Simulation

using DrWatson
@quickactivate "Agrimate"

include(srcdir("simulation.jl"))


simulate(
    Params(
        τ_P = 1000.
    );
    t_max = 6 * 24, 
    verbose = false,
    use_cache = false,
    source = @source
)

end