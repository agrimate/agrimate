module Simulation

using DrWatson
@quickactivate "Agrimate"

include(srcdir("simulation.jl"))

using MPI

!MPI.Initialized() && MPI.Init()

comm = MPI.COMM_WORLD
n_workers = MPI.Comm_size(comm)
worker_id = MPI.Comm_rank(comm) + 1

if worker_id == 1
    @info "Number of workers" n_workers
    flush(stderr)
end

# ******************************************************************************************

# Scenario

scenario_params = (;
    crops = "wheat",
    baseline = "2004-2006",
    start = Date("2004-01-01"),
    regions = "AgrimateEU28",
    extra_regions = Dict("Egypt" => "EGY"),
    λ = 1.0,
    ψ = :empirical,
    A_d_star = :empirical,
    A_c_star = :empirical,
    production_anomalies = "FAO",
    export_restrictions = "2007-2011",
    production_cutoff = 0,
)
default_scenario_params = Params(; scenario_params...)

t_max = 9 * 24 # 5 * N_year

# Parameters to explore

param_space = Dict(
    :crops => [
        "wheat",
        # "rice",
        # "maize",
    ],
    :start => [
        Date("2004-01-01"),
        # Date("2007-01-01"),
    ],
    :extra_regions => [
        Dict("Egypt" => "EGY"),
        # Dict("Kenya" => "KEN"),
    ],
    :baseline => [
        @onlyif(:start == Date("2004-01-01"), "2004-2006"),
        # @onlyif(:start == Date("2007-01-01"), "2007-2009"),
    ],
    :export_restrictions => [nothing, "2007-2011"],
)

extra_param_space = Dict(
    :production_anomalies => ["FAO", "USDA"],
    :N_year => [24, 36, 48],
    :N_del => [1, 2, 3] .* (default_scenario_params.N_year ÷ 12),
    :N_hor => [1, 2, 3] .* default_scenario_params.N_year,
    :N_for => [0, 3, 6] .* (default_scenario_params.N_year ÷ 12),
    :α => [4.0, 5.0, 6.0],
    :β => [0.01, 0.05, 0.10],
    :p_sto => [0.05, 0.1, 0.15],
    :σ => [1.5, 2.0, 2.5],
    :τ_a => [0.05, 0.2, 0.5],
    :τ_P => [0.1, 0.2, 0.5],
    :τ_exp => [0.2, 0.5, 1.0],
    :τ_for => [0.1, 0.2, 0.5],
    :τ => [0.1, 0.2, 0.5],
    :ι => [0.001, 0.005, 0.01],
    :ε_c => [0.05, 0.1, 0.2],
    :ψ => [Symbol("empirical_EGY=$v") for v in [0.1, 0.5, 1.0]],
    :A_d_star => [Symbol("empirical_EGY=$v") for v in [0.01, 0.2, 0.8]],
    :A_c_star => [Symbol("empirical_EGY=$v") for v in [0.01, 0.2, 0.8]],
    :flow_cutoff => [0.005, 0.01, 0.02],
)

# Create vec of params

basic_param_dicts = dict_list(param_space)
param_dicts = [basic_param_dicts...]
for (param, values) in extra_param_space
    for value in values
        for basic_param_dict in basic_param_dicts
            value == getproperty(default_scenario_params, param) && continue
            push!(param_dicts, merge(basic_param_dict, Dict(param => value)))
        end
    end
end

params_vec = Params[]

for param_dict in param_dicts
    params = merge(scenario_params, (; param_dict...))
    params = Params(; params...)
    push!(params_vec, params)
end

# ******************************************************************************************

n_tasks = length(params_vec)
if worker_id == 1
    @info "Number of tasks" n_tasks
    flush(stderr)
end
worker_task_ids = [collect(i:n_workers:n_tasks) for i in 1:n_workers]

missed_params_vec = Params[]

for params in params_vec[worker_task_ids[worker_id]]
    @info "Worker ID" worker_id
    # simulate(params; use_cache = true, t_max, verbose = false, source = @source)
    try
        simulate(
            params;
            use_cache = false,
            t_max = (t_max * params.N_year) ÷ default_scenario_params.N_year,
            verbose = false,
            source = @source,
            processid = worker_id,
        )
    catch error
        push!(missed_params_vec, params)
        @error "SIMULATION TERMINATED" error
    end
    flush(stderr)
    flush(stdout)
end

if length(missed_params_vec) > 0
    for missed_params in missed_params_vec
        @warn "Missed params" missed_params
    end
    flush(stderr)
    flush(stdout)
end

end