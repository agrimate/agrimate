using DrWatson
@quickactivate "Agrimate"

include(srcdir("plot.jl"))
include(srcdir("plot_geo.jl"))

make_countries_geodata()
make_regions_plot(
    "AgrimateEU28";
    extra_regions = Dict("Egypt" => "EGY"),
    extra_region_colors = "Egypt" => "grey",
)