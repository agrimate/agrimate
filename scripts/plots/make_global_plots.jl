using DrWatson
@quickactivate "Agrimate"

using DataFrames
using Dates: Date

include(srcdir("io.jl"))
include(srcdir("params.jl"))
include(srcdir("plot.jl"))

destdir(args...) = plotsdir("global", args...)

function plot_prices()
    df = wload(datadir("clean", "prices", "commodity_prices.csv"))
    df = select(
        df,
        :Year,
        :Month,
        "Wheat, US HRW (deflated)" => :Wheat,
        "Maize, US (deflated)" => :Maize,
        "Rice, Thai 5% (deflated)" => :Rice,
    )
    df = stack(df, Not([:Year, :Month]); variable_name = :Crop, value_name = :Price)
    wsave(destdir("data", "prices.csv"), df)

    df[!, :Date] = Date.(df.Year, df.Month)
    years = unique(df.Year) |> sort

    theme = set_my_theme!(; linewidth = 1, markersize = 5)
    linepatchsize = theme.linepatchsize[]

    crop_sorted = :Crop => sorter("Wheat", "Maize", "Rice")
    plt = (
        data(df) *
        visual(Lines) *
        mapping(:Date => "", :Price => "Price (USD in 2000 / tonne)") *
        mapping(; color = crop_sorted, linestyle = crop_sorted)
    )

    xticks = datetimeticks([Date("$y-01-01") for y in years], ["$y" for y in years])
    xticklabelrotation = π / 3
    axis = (; xticks, xticklabelrotation, width = 180, height = 120)
    legend = (; patchsize = linepatchsize)
    palettes = (;)

    fig, grid = draw(plt; axis, legend, palettes)

    setticksvisible!(grid)
    resize_to_layout!(fig)

    wsave(destdir("prices.png"), fig; px_per_inch = 300)

    return fig
end

function plot_stu()
    dfs = []
    for crop in ["wheat", "maize", "rice"]
        df_stocks = wload(
            datadir(
                "clean",
                "timeseries",
                savename("stocks", (; crop, source = "USDA"), "csv"; savename_kwargs...),
            ),
        )
        df_use = wload(
            datadir(
                "clean",
                "timeseries",
                savename(
                    "domestic_supply",
                    (; crop, source = "USDA"),
                    "csv";
                    savename_kwargs...,
                ),
            ),
        )

        df_stocks = combine(
            groupby(df_stocks, :Year),
            "Ending Stocks" => sum ∘ skipmissing => :Stocks,
        )
        df_stocks[!, :Year] .+= 1
        df_use = combine(
            groupby(df_use, :Year),
            "Domestic supply quantity" => sum ∘ skipmissing => :Use,
        )
        # Use Begining stocks
        df = innerjoin(df_stocks, df_use; on = :Year)
        df[!, :STU] .= df.Stocks ./ df.Use
        df[!, :Crop] .= uppercasefirst(crop)
        push!(dfs, select(df, :Year, :Crop, :STU))
    end

    df = vcat(dfs...)

    df = subset(df, :Year => y -> 2000 .≤ y .≤ 2022)

    wsave(destdir("data", "stock-to-use.csv"), df)

    theme = set_my_theme!(; linewidth = 1, markersize = 5)
    linepatchsize = theme.linepatchsize[]

    years = unique(df.Year) |> sort

    crop_sorted = :Crop => sorter("Wheat", "Maize", "Rice")
    plt = (
        data(df) *
        (visual(Scatter) + visual(Lines)) *
        mapping(:Year => "", :STU => "Stock To Use Ratio") *
        mapping(; color = crop_sorted, marker = crop_sorted, linestyle = crop_sorted)
    )

    # xticks = datetimeticks([y for y in years], ["$y" for y in years])
    xticks = collect(years)
    xticklabelrotation = π / 3
    axis = (; xticks, ylims = (0, 0.5), xticklabelrotation, width = 180, height = 120)
    legend = (; patchsize = linepatchsize)
    palettes = (;)

    fig, grid = draw(plt; axis, legend, palettes)
    for g in grid
        ylims!(g.axis, axis.ylims)
    end

    setticksvisible!(grid)
    resize_to_layout!(fig)

    wsave(destdir("stock-to-use.png"), fig; px_per_inch = 300)

    return fig
end

function plot_production(; region = "world")
    dfs = []
    for crop in ["wheat", "maize", "rice"]
        df = wload(
            datadir(
                "clean",
                "timeseries",
                savename("production", (; crop, source = "USDA"), "csv"; savename_kwargs...),
            ),
        )
        df[!, :Crop] .= uppercasefirst(crop)
        push!(dfs, select(df, "Area", "Year", "Crop", "Production Trend", "Production"))
    end
    df = vcat(dfs...)
    df = subset(df, :Year => y -> 2000 .≤ y .≤ 2022)

    if region == "hoa"
        areas = ["BDI", "DJI", "ERI", "ETH", "KEN", "RWA", "SDN", "SOM", "SSD", "TZA", "UGA"]
        df = subset(df, :Area => a -> in(areas).(a))
        println("Areas", unique(dropmissing(df).Area))
    end
    df = combine(
        groupby(df, [:Year, :Crop]),
        "Production Trend" => sum ∘ skipmissing => :Trend,
        "Production" => sum ∘ skipmissing => :Production,
    )
    df[!, [:Production, :Trend]] ./= 1000
    
    wsave(destdir("data", "production;region=$region.csv"), df)

    theme = set_my_theme!(; linewidth = 1, markersize = 5)
    linepatchsize = theme.linepatchsize[]

    years = unique(df.Year) |> sort

    df[!, :mid] .= (df.Production .+ df.Trend) ./ 2
    df[!, :high] .= max.(df.Production, df.Trend) .- df.mid
    df[!, :low] .= df.mid .- min.(df.Production, df.Trend)

    crop_sorted = :Crop => sorter("Wheat", "Maize", "Rice")
    plt = (
        data(df) *
        (
            visual(Scatter) * mapping(:Year => "", :Production)  + 
            visual(Lines) * mapping(:Year => "", :Trend) +
            visual(Errorbars) * mapping(:Year => "", :mid => "", :low => "", :high => "")
        ) *
        mapping(; color = crop_sorted, marker = crop_sorted, linestyle = crop_sorted)
    )

    # xticks = datetimeticks([y for y in years], ["$y" for y in years])
    xticks = collect(years)
    xticklabelrotation = π / 3
    axis = (; xticks, xticklabelrotation, width = 180, height = 120, ylabel = "Production (mln tonnes)")
    legend =  (; patchsize = linepatchsize)
    palettes = (;)

    fig, grid = draw(plt; axis, legend, palettes)


    setticksvisible!(grid)
    resize_to_layout!(fig)

    wsave(destdir("production;region=$region.png"), fig; px_per_inch = 300)

    return fig
end

# plot_prices()
# plot_stu()
plot_production()
plot_production(; region = "hoa")