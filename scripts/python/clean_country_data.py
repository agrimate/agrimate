import os
import pandas as pd

from AgriculturalData.config import data_dir

from AgriculturalData.data_cleaning import clean_fao
from AgriculturalData.data_cleaning import remove_aggregate_areas, reverse_stock_variation_sign, convert_rice_milled_to_paddy



def clean_fao_country_data(country, years, force=False):

    os.makedirs(data_dir("clean", "fao"), exist_ok=True)

    dest = data_dir("clean", "fao", f"fao_country_data_{country}.csv")
    if dest.exists() and not force:
        return

    df_codes =  pd.read_csv(data_dir("clean", "fao", f"area_codes_fao.csv"))
    area_codes = df_codes.loc[df_codes["ISO3 Code"] == country, "Area Code"].tolist()

    print(f"Cleaning FAO data for {country}, area code = {area_codes}")
    assert len(area_codes) > 0

    elements_fbs = [
        "Production", 
        "Import Quantity", 
        "Export Quantity",
        "Domestic supply quantity",
        "Stock Variation",
        "Food",
        "Food supply quantity (kg/capita/yr)",
        "Food supply (kcal/capita/day)",
        "Protein supply quantity (g/capita/day)",
        "Fat supply quantity (g/capita/day)",
        "Total Population - Both sexes", 
    ]

    columns_fbs = ["Year", "Area", "Area Code", "Item", "Item Code", "Element", "Element Code", "Value", "Unit", "Flag"]

    df1 = clean_fao(
        filename="FoodBalanceSheetsHistoric_E_All_Data_(Normalized).csv",
        area_codes=area_codes, 
        years=[y for y in years if y < 2014],
        elements=elements_fbs,
        columns=columns_fbs,
        extra_funcs=[remove_aggregate_areas, reverse_stock_variation_sign, convert_rice_milled_to_paddy],
    )
    df2 = clean_fao(
        filename="FoodBalanceSheets_E_All_Data_(Normalized).csv",
        area_codes=area_codes, 
        years=[y for y in years if y >= 2014],
        elements=elements_fbs,
        columns=columns_fbs,
        extra_funcs=[remove_aggregate_areas],
    )
    df3 = clean_fao(
        filename="Production_Crops_Livestock_E_All_Data_(Normalized).csv",
        area_codes=area_codes, 
        years=years,
        elements=["Production", "Yield"],
        columns=columns_fbs,
        extra_funcs=[remove_aggregate_areas],
    )

    df = pd.concat([df1, df2, df3])
    df = df.sort_values(["Year", "Item", "Element"])

    df.to_csv(dest, index=False)


def clean_usda_country_data(country, years, force=False):

    os.makedirs(data_dir("clean", "usda"), exist_ok=True)

    dest = data_dir("clean", "usda", f"usda_country_data_{country}.csv")
    if dest.exists() and not force:
        return

    df_codes = pd.read_csv(data_dir("clean", "usda", f"country_codes_usda.csv"))
    country_codes = df_codes.loc[df_codes["ISO3 Code"] == country, "USDA Code"].tolist()

    print(f"Cleaning USDA data for {country}, country code = {country_codes}")
    assert len(country_codes) > 0

    filenames = ["psd_alldata"]
    dfs = []
    for filename in filenames:
        file = data_dir("raw", "usda", filename + ".csv")
        df = pd.read_csv(file)
        df = df[df["Country_Code"].isin(country_codes)]
        df = df[df["Market_Year"].isin(years)]
        dfs.append(df)
    df = pd.concat(dfs)

    df.to_csv(dest, index=False)


def clean_hoa_data(force=False):
    
    years = list(range(1970, 2022))
    countries = ["BDI", "DJI", "ERI", "ETH", "KEN", "RWA", "SDN", "SOM", "SSD", "TZA", "UGA"]

    for country in countries:
        print(country)
        clean_fao_country_data(country, years, force=force)
        clean_usda_country_data(country, years, force=force)




if __name__ == "__main__":
    
    clean_hoa_data(force=True)