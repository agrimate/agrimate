import os
import pandas as pd
from icecream import ic

from AgriculturalData.config import data_dir


def clean_policy_data(force=False):

    os.makedirs(data_dir("clean", "policies"), exist_ok=True)

    products = ["agricultural products", "agricultural commodities", "cereals", "grains", "flour", "exports"]

    for crop in ["wheat", "rice", "maize"]:

        file = data_dir("clean", "policies", f"{crop}_policies_ifpri.csv")
        if file.exists() and not force:
            return

        df = pd.read_csv(data_dir("raw", "ifpri", "List2008_data.csv"))

        df = df.rename(columns={"Day of Starting Date": "Start", "Day of End Date": "End", "Country Label": "Country"})
        for col in ["Start", "End"]:
            df[col] = pd.to_datetime(df[col])
        df["Duration"] = (df["End"] - df["Start"]).dt.days
        df = df[df["Duration"] > 0]

        df_codes = pd.read_csv(data_dir("clean", "fao", "area_codes_fao.csv"))
        df_codes = df_codes.rename(columns={"Area": "Country", "ISO3 Code": "Country Code"})[["Country", "Country Code"]]
        df_codes = df_codes.set_index("Country Code")
        for country, code in [("Bolivia", "BOL"), ("Tanzania", "TZA"), ("Russia", "RUS"), ("Iran", "IRN"), ("Syria", "SYR")]:
            df_codes.loc[code] = country
        df_codes = df_codes.reset_index()
        df = pd.merge(df, df_codes, on="Country", how="left")

        columns = ["Country Code", "Country", "Start", "End", "Duration", "Category", "Products", "Description", "Source"]
        df = df[columns].drop_duplicates()

        product_filter = df["Products"].str.contains(crop)
        if crop == "maize":
            product_filter |= df["Products"].str.contains("corn")
            product_filter |= df["Description"].str.contains("corn")
            product_filter |= df["Description"].str.contains("maize")

        for product in products:
            product_filter |= df["Products"].str.contains(product)

        df = df[product_filter]

        df = df.sort_values(by=["Country Code", "Start", "End"])
        df = df.reset_index(drop=True)

        df.to_csv(file, index_label="ID")



if __name__ == "__main__":

    clean_policy_data(force=True)
