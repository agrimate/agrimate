import numpy as np
import os
import pandas as pd

from AgriculturalData.config import data_dir
from AgriculturalData.harvest_distributions import raised_cosine_harvest_distribution


def generate_harvest_distributions(crop, force=False):

    os.makedirs(data_dir("clean", "harvest_distributions"), exist_ok=True)
    file = data_dir("clean", "harvest_distributions", f"harvest_distributions;crop={crop}.csv")
    if not force and file.exists():
        return

    df = pd.read_csv(
        data_dir("clean", "crop_calendars", f"{crop}_crop_calendar.csv"),
        usecols=["ISO3 Code", "Harvest Start", "Harvest End", "Country Share"],
    )

    distributions = {}
    for area in df["ISO3 Code"].unique():
        # Drop EU27 entry
        if area == "EU27":
            continue
        distributions[area] = np.zeros(365)
        for id, row in df[df["ISO3 Code"] == area].iterrows():
            share = row["Country Share"]
            start_day = row["Harvest Start"]
            end_day = row["Harvest End"]
            distributions[area] += share * raised_cosine_harvest_distribution(start_day, end_day)
        # Check normalization
        distributions[area] = distributions[area].round(5)
        assert (abs(sum(distributions[area]) - 1) < 5e-3)

    df = pd.DataFrame(distributions).transpose()
    df.index.name = "Area"
    df.columns = list(range(1, 366))
    df = df.reset_index()
    df.to_csv(file, index=False)


def generate_harvest_distributions_all(force=False):

    crops = ["wheat", "rice", "maize"]
    for crop in crops:
        generate_harvest_distributions(crop, force=force)

    
if __name__ == "__main__":

    generate_harvest_distributions_all()