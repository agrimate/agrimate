import numpy as np
import os
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
import seaborn as sns
from icecream import ic

from AgriculturalData.config import data_dir, plots_dir

def plot_timeseries(crop, variable):

    variable_label = {
        "production": "Production", 
        "domestic_supply": "Domestic supply quantity",
        "stocks": "Stock Variation",
    }
    label = variable_label[variable]

    os.makedirs(plots_dir(variable, crop), exist_ok=True)

    dfs = []
    for source in ["FAO", "USDA"]:
        df = pd.read_csv(data_dir("clean", "timeseries", f"{variable};crop={crop};source={source}.csv"))
        df["Source"] = source
        df = df.melt(id_vars = ["Area", "Year", "Source"], var_name="Variable", value_name="Value")
        dfs.append(df)
    df = pd.concat(dfs)

    vars = [label]
    if variable != "stocks":
        vars += [f"{label} Trend"]
    df = df[df["Variable"].isin(vars)]

    gdf = df.groupby(by="Area")




    for area, df in gdf:

        print(f"Plotting {label} | {crop} | {area}")

        df = df.drop(columns="Area")
        df = df.reset_index(drop=True)
        df["Value"] /= 1000

        fig, ax = plt.subplots(figsize=(8, 5))
        sns.lineplot(data=df, x="Year", y="Value", hue="Source", style="Variable", markers=True, ax=ax)
        ax.set(
            title=f"{crop.capitalize()} {label} {area}",
            ylabel=f"{label} (mln tonnes)",
            xlabel=None,
        )
        sns.despine()
        ax.legend(bbox_to_anchor=(1.02, 1), loc="upper left", borderaxespad=0)
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        fig.tight_layout()
        fig.savefig(plots_dir(variable, crop, f"{variable};area={area};crop={crop}.png"), dpi=100)

        plt.close()
        







if __name__ == "__main__":

    sns.set_theme(style="whitegrid")
    for crop in ["wheat", "maize", "rice"]:
        plot_timeseries(crop, "production")
        plot_timeseries(crop, "domestic_supply")
        plot_timeseries(crop, "stocks")