import os
import pandas as pd
from icecream import ic

from AgriculturalData.config import data_dir


def create_policy_data(force=False):

    products = ["agricultural products", "agricultural commodities", "cereals", "grain", "flour", "exports"]

    df_impact = pd.Series({"Actual Ban": 0.95, "Export Taxes": 0.5, "Export Licensing": 0.5}, name="Policy Impact")

    for crop in ["wheat", "rice", "maize"]:

        file = data_dir("clean", "policies", f"{crop}_policies.csv")
        if file.exists() and not force:
            return

        df = pd.read_csv(data_dir("clean", "policies", f"{crop}_policies_ifpri.csv"))

        df = pd.merge(df, df_impact, left_on="Category", right_index=True, how="left")

        df = df.set_index("ID")
        df["IDs"] = df.index.astype("str")
        df["Note"] = ""

        if crop == "wheat":
            df = df[df.index != 1]
            df.loc[2, ["End", "Note"]] = ["2008-05-13", "end shifted to avoid overlap"]
            df.loc[6, ["End", "Note", "IDs"]] = ["2009-03-01", "end shifted to merge policies", "6,7,8"]
            df = df[df.index != 7]
            df = df[df.index != 8]
            df.loc[11, ["Start", "Note"]] = ["2007-02-01", "start shifted to agree with Headey 2011"]
            df.loc[13, ["End", "Note"]] = ["2008-03-31", "end shifted to avoid overlap"]
            df.loc[19, ["Start", "Note"]] = ["2008-11-02", "start shifted to avoid overlap"]
            df.loc[210] = df.loc[21]
            df.loc[21, ["End", "Note"]] = ["2010-07-31", "ID 21 split into tax and ban"]
            df.loc[210, ["Start", "Policy Impact", "Category", "Note"]] = [
                "2010-08-01", df_impact.loc["Actual Ban"], "Actual Ban", "ID 21 split into tax and ban"
            ]

        if crop == "maize":
            df = df[df.index != 1]
            df = df[df.index != 4]
            df = df[~df.index.isin([6,7])]
            df.loc[8, ["Start", "Note", "IDs"]] = ["2009-01-01", "start shifted to avoid overlap", "6,7,8"]
            df = df[df.index != 14]
            df.loc[18, ["Start", "Policy Impact", "Category", "Note"]] = [
                "2010-08-01", df_impact.loc["Actual Ban"], "Actual Ban","start shifted to align with the ban period"
            ]

        if crop == "rice":
            df.loc[1, ["Policy Impact", "Note"]] = [
                df_impact["Export Licensing"], "policy impact reduced because the ban did not include Brazil"
            ]
            df = df[df.index != 2]
            df.loc[300] = df.loc[3]
            df.loc[3, ["Start", "End", "Note"]] = ["2008-05-01", "2008-11-01", "period harmonized with description"]
            df.loc[300, ["Start", "End", "Note"]] = ["2009-12-01", "2010-12-01", "period harmonized with description"]  
            df = df[df.index != 4]
            df = df[df.index != 7]
            df.loc[8, ["Start", "Note", "IDs"]] = ["2009-01-01", "start shifted to avoid overlap", "7,8"]
            df.loc[14, ["End", "Note", "IDs"]] = ["2011-09-01", "end date shifted to merge policies", "14,15,16,17,18"]
            df = df[~df.index.isin([15,16,17,18])]
            df = df[df.index != 20]
            df = df[df.index != 30]


        for col in ["Start", "End"]:
            df[col] = pd.to_datetime(df[col])
        df["Duration"] = (df["End"] - df["Start"]).dt.days

        
        df = df[["Country Code", "Country" ,"Start", "End", "Duration", "Policy Impact", "Category", "IDs", "Note"]]
        df = df.sort_values(by=["Country Code", "Start", "End"])

        df.to_csv(file, index=False)



if __name__ == "__main__":

    create_policy_data(force=True)
