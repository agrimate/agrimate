import os
import pandas as pd
import numpy as np
from icecream import ic

from AgriculturalData.data_cleaning import clean_fao
from AgriculturalData.item_groups import item_groups_fao, item_group_codes_fao
from AgriculturalData.config import data_dir


def load_income_group_data(years):
    df_grp = pd.read_excel(
        data_dir("raw", "world_bank", "OGHIST.xlsx"), 
        sheet_name="Country Analytical History", 
        skiprows=[0, 1, 2, 3, 4, 6, 7, 8, 9, 10]
    )
    cols = list(df_grp.columns)
    cols[0] = "ISO3 Code"
    cols[1] = "Area"
    df_grp.columns = cols

    df_grp = df_grp.melt(
        id_vars=["ISO3 Code", "Area"], 
        value_vars=[y for y in years], 
        var_name="Year", 
        value_name="Income Group"
    )
    
    df_grp = df_grp.dropna()
    return df_grp


def create_budget_data(crop, force=False):

    os.makedirs(data_dir("clean", "budget"), exist_ok=True)

    file = data_dir("clean", "budget", f"budget;crop={crop}.csv")
    if file.exists() and not force:
        return

    years = list(range(2000, 2021))

    # FAO data
    index_cols = ["Area Code", "Year"]
    
    df_imp = pd.read_csv(data_dir("clean", "fao", f"{crop}_trade.csv"))
    df_imp = df_imp[df_imp["Element"] == "Import Value"]
    df_imp = df_imp[index_cols + ["Value"]]
    df_imp = df_imp.dropna().groupby(by=index_cols).agg({"Value": sum}).reset_index()
    df_imp = df_imp.rename(columns={"Value": f"Import Value ({crop})"})

    df_prv = pd.read_csv(data_dir("clean", "fao", f"{crop}_production_value.csv"))
    df_prv = df_prv[index_cols + ["Value"]]
    df_prv = df_prv.rename(columns={"Value": f"Production Value ({crop})"})

    df = pd.merge(df_prv, df_imp, how="outer", on=index_cols)
    area_codes = pd.read_csv(data_dir("clean", "fao", "area_codes_fao.csv"), usecols=["Area", "Area Code", "ISO3 Code"])
    df = pd.merge(df, area_codes, on="Area Code", how="left")

    df_fbs = pd.read_csv(data_dir("clean", "food_balances", f"{crop}_food_balance_fao.csv"))
    df_fbs[f"Non-exported Production Share ({crop})"] = np.maximum(1 - df_fbs["Export Quantity"] / df_fbs["Production"], 0)
    df_fbs = df_fbs.rename(columns={"Production": f"Production ({crop})", "Import Quantity": f"Import Quantity ({crop})"})
    df_fbs = df_fbs[["ISO3 Code", "Year", f"Production ({crop})", f"Import Quantity ({crop})", f"Non-exported Production Share ({crop})"]]

    df = pd.merge(df, df_fbs, how="outer", on=["ISO3 Code", "Year"])
    df = df[df["Year"].isin(years)]

    df = df[~df["ISO3 Code"].isin(["BLX", "CSK", "ANT", "BMU"])]
    df = df[~df["Area"].isin(["Ethiopia PDR", "USSR", "Yugoslav SFR"])]
    df = df[~((df["Area"] == "Sudan") & (df["Year"] < 2012))]
    df = df[~((df["Area"] == "Sudan (former)") & (df["Year"] >= 2012))]
    df = df[~((df["ISO3 Code"] == "SCG") & (df["Year"] >= 2006))]


    # World Bank data
    filename = "API_TX.VAL.MRCH.CD.WT_DS2_en_csv_v2_3731336.csv"
    df_exp = pd.read_csv(data_dir("raw", "world_bank", filename), skiprows=4)

    df_exp = df_exp.melt(
        id_vars=["Country Code"], 
        value_vars=[str(y) for y in years], 
        var_name="Year", 
        value_name="Merchandise Export Value"
    )
    df_exp["Year"] = df_exp["Year"].astype(int)
    df_exp["Merchandise Export Value"] /= 1000 # convert to 1000 USD

    df = pd.merge(df, df_exp.rename(columns={"Country Code": "ISO3 Code"}), on=["ISO3 Code", "Year"], how="left")


    df_grp = load_income_group_data(years)
    df = pd.merge(df, df_grp.drop(columns="Area"), on=["ISO3 Code", "Year"], how="left")


    columns = [
        "Income Group", 
        f"Import Quantity ({crop})", f"Import Value ({crop})", 
        f"Production ({crop})", f"Production Value ({crop})", f"Non-exported Production Share ({crop})", 
        "Merchandise Export Value"
    ]
    index_cols = ["ISO3 Code", "Area", "Year"]
    df = df[index_cols + columns].sort_values(by=["ISO3 Code", "Year"])

    # Impute

    prod_value_isna = df[f"Production Value ({crop})"].isna()
    prod_iszero = df[f"Production ({crop})"] == 0.
    df.loc[prod_value_isna, f"Production Value ({crop})"] = (
        df[f"Production ({crop})"] * df[f"Import Value ({crop})"] / df[f"Import Quantity ({crop})"]
    )
    df.loc[prod_value_isna & ~prod_iszero, "Notes"] = "Production Value imputed"
    df.loc[prod_iszero, f"Non-exported Production Share ({crop})"] = np.nan


    # Calculate rolling sums
    df[f"Non-exported Production Value ({crop})"] = np.where(
        df[f"Production ({crop})"] == 0, 
        0,
        df[f"Production Value ({crop})"] * df[f"Non-exported Production Share ({crop})"] 
    )

    df = df.reset_index(drop=True)
    df_rol = df.groupby(by="ISO3 Code", sort="True").rolling(3, center=True, min_periods=3).sum().reset_index()

    df["A_d (3-year window)"] = (
        (df_rol[f"Non-exported Production Value ({crop})"] + df_rol[f"Import Value ({crop})"]) 
        / (df_rol[f"Non-exported Production Value ({crop})"] + df_rol[f"Merchandise Export Value"])
    )


    df = df[index_cols + columns + ["A_d (3-year window)", "Notes"]]
    
    df.to_csv(file, index=False, float_format='%.4f')

def create_budget_data_all(force=False):

    for crop in ["wheat", "maize", "rice"]:
        create_budget_data(crop, force=force)



def create_household_spending_data(force=False):

    os.makedirs(data_dir("clean", "budget"), exist_ok=True)

    file = data_dir("clean", "budget", f"food_spending.csv")
    if file.exists() and not force:
        return

    years = list(range(2015,2021))

    dfs = []

    for year in years:
        df = pd.read_excel(
            data_dir("raw", "usda", "food_spending.xlsx"), 
            sheet_name=str(year), 
            skiprows=6,
            header=None,
            names=["Area", "Food Spending Share"],
            usecols=[0, 1],
        )
        df["Food Spending Share"] /= 100
        df["Year"] = year
        df = df.dropna()
        dfs.append(df)

    df = pd.concat(dfs)

    df_grp = load_income_group_data(years)

    # Fix some area names so they match income group data
    areas = {
        "USA": "United States",
        "South Korea": "Korea, Rep.",
        "Taiwan": "Taiwan, China",
        "Hong Kong, China": "Hong Kong SAR, China",
        "Slovakia": "Slovak Republic",
        "Venezuela": "Venezuela, RB",
        "Iran": "Iran, Islamic Rep.",
        "Russia": "Russian Federation",
        "Egypt": "Egypt, Arab Rep.",
        "Laos": "Lao PDR",
        "Bosnia-Herzegovina": "Bosnia and Herzegovina",
        "Macedonia": "North Macedonia",
    }
    df["Area"] = df["Area"].replace(areas)

    df = pd.merge(df, df_grp, on=["Area", "Year"], how="outer")

    df.loc[df["Area"] == "Venezuela, RB", "ISO3 Code"] = "VEN"

    df["Year"] = df["Year"].astype(int)

    # Impute data based on income group
    share_median = df.groupby(["Income Group", "Year"]).agg({"Food Spending Share": np.median})
    share_missing = df["Food Spending Share"].isna()

    df_imp = pd.merge(df[share_missing].drop(columns="Food Spending Share"), share_median, on=["Income Group", "Year"], how="left")
    df["Notes"] = ""
    df_imp["Notes"] = "Food Spending Share imputed"

    df = pd.concat([df[~share_missing], df_imp])

    df = df[["ISO3 Code", "Area", "Year", "Income Group", "Food Spending Share", "Notes"]]
    df = df.sort_values(by=["ISO3 Code", "Year"])

    df.to_csv(file, index=False, float_format='%.4f')




if __name__ == "__main__":

    create_budget_data_all()
    create_household_spending_data()