from AgriculturalData.config import data_dir
from AgriculturalData.downloads import download


def download_world_bank_pink_sheet(force=False):

    download(
        url="https://thedocs.worldbank.org/en/doc/5d903e848db1d1b83e0ec8f744e55570-0350012021/related/CMO-Historical-Data-Monthly.xlsx",
        savedir=data_dir("raw", "world_bank"),
        filename="CMO-Historical-Data-Monthly.xlsx",
        force=force,
    )

    # Alternatives: 
    # IMF data https://www.imf.org/en/Research/commodity-prices
    # USDA data https://www.ers.usda.gov/data-products/wheat-data/


# Download manually CPI-U data https://data.bls.gov/timeseries/CUUR0000SA0?years_option=all_years to data/raw/bls/cpi-u.xlsx

if __name__ == "__main__":

    download_world_bank_pink_sheet()