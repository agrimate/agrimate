import os
from unicodedata import name
import pandas as pd
from icecream import ic

from AgriculturalData.config import data_dir


def clean_price_data(force=True):

    os.makedirs(data_dir("clean", "prices"), exist_ok=True)

    file = data_dir("clean", "prices", "commodity_prices.csv")
    if file.exists() and not force:
        return

    
    year_min, year_max = 2000, 2022

    columns = {
        "MAIZE": "Maize, US", 
        "RICE_05": "Rice, Thai 5%", 
        "WHEAT_US_SRW": "Wheat, US SRW",
        "WHEAT_US_HRW": "Wheat, US HRW",
    }

    df = pd.read_excel(
        data_dir("raw", "world_bank", "CMO-Historical-Data-Monthly.xlsx"),
        sheet_name="Monthly Prices", skiprows=6, index_col=0, na_values=".."
    )
    df = df[columns.keys()].rename(columns=columns)[~df.index.isna()]
    df.index.name = "Date"
    df = df.reset_index()
    df["Year"] = df["Date"].str[:4].astype(int)
    df["Month"] = df["Date"].str[5:].astype(int)

    df_cpi = pd.read_excel(
        data_dir("raw", "bls", "cpi-u.xlsx"), skiprows=11,
    )
    df_cpi = df_cpi.iloc[:, :13]

    df_cpi.columns = ["Year"] + list(range(1, 13))
    df_cpi = pd.melt(df_cpi, id_vars="Year", var_name="Month", value_name="CPI")
    # Normalize CPI by the value for Jan - year_min
    df_cpi["CPI"] /= df_cpi.loc[(df_cpi["Year"] == year_min) & (df_cpi["Month"] == 1), "CPI"].iloc[0]


    df = pd.merge(df, df_cpi, on=["Year", "Month"], how="inner")

    df = df[(year_min <= df["Year"]) & (df["Year"] <= year_max)]
    for crop in columns.values():
        df[f"{crop} (deflated)"] = df[crop] / df["CPI"]

    columns = ["Year", "Month"] + [col for col in columns.values()] + [f"{col} (deflated)" for col in columns.values()] + ["CPI"]
    df = df[columns]

    df.to_csv(file, index=False, float_format='%.3f')


if __name__ == "__main__":

    clean_price_data()