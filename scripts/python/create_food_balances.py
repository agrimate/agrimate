from textwrap import indent
import pandas as pd
import numpy as np
import os

from AgriculturalData.config import data_dir
from AgriculturalData.data_processing import impute_food_balance_fao


def create_food_balances_fao(force=False):

    os.makedirs(data_dir("clean", "food_balances"), exist_ok=True)

    for crop in ["wheat", "rice", "maize"]:
        file = data_dir("clean", "food_balances", f"{crop}_food_balance_fao.csv")
        if file.exists() and not force:
            continue
        df = impute_food_balance_fao(crop)
        # Drop rows for Czechia and Slovakia for year 1992
        df = df[~(df["ISO3 Code"].isin(["CZE", "SVK"]) & (df["Year"] == 1992))]
        df.to_csv(file, index=False)
    

if __name__ == "__main__":

    create_food_balances_fao()


