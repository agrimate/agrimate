from calendar import c
import os
import pandas as pd
from itertools import count, product
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
from pytz import country_names
import seaborn as sns

from AgriculturalData.config import data_dir, plots_dir
from AgriculturalData.detrending import detrend_timeseries




def process_country_data(country, force=False):

    os.makedirs(data_dir("clean", "country_data"), exist_ok=True)

    file = data_dir("clean", "country_data", f"fao_country_data;country={country}.csv")
    if not force and file.exists():
        return 

    df = pd.read_csv(data_dir("clean", "fao", f"fao_country_data_{country}.csv"))

    # Create population timeseries
    df_pop = df[df["Element Code"] == 511].rename(columns={"Value": "Population"})
    df_pop["Population"] *= 1000
    df_pop = df_pop[["Year", "Population"]]

    # Select only FBS food items and population
    df = df[(2510 < df["Item Code"]) & (df["Item Code"] < 2900)]
    if len(df) == 0:
        return

    df = df.drop(columns=["Unit", "Flag"])
    index = ["Year", "Item", "Item Code"]
    df = df.pivot_table(index=index, columns="Element", values="Value", fill_value=0).reset_index()

    df = df[df["Domestic supply quantity"] > 0]

    days_in_year = 365.25
    df["kcal/kg"] = df["Food supply (kcal/capita/day)"] * days_in_year / df["Food supply quantity (kg/capita/yr)"]
    df["protein g/kg"] = df["Protein supply quantity (g/capita/day)"] * days_in_year / df["Food supply quantity (kg/capita/yr)"]
    df["fat g/kg"] = df["Fat supply quantity (g/capita/day)"] * days_in_year / df["Food supply quantity (kg/capita/yr)"]
    df["food/supply"] = df["Food"] / df["Domestic supply quantity"]
    df["(production+imports)/supply"] = (df["Production"] + df["Import Quantity"]) / df["Domestic supply quantity"]
    df["production/(production+imports)"] = df["Production"] / (df["Production"] + df["Import Quantity"])
    df["imports/(production+imports)"] = df["Import Quantity"] / (df["Production"] + df["Import Quantity"])
    
    df["Estimated Food Produced"] = df["Food"] * df["production/(production+imports)"]
    df["Estimated Food Imported"] = df["Food"] * df["imports/(production+imports)"]

    df["Estimated Supply Produced"] = df["Domestic supply quantity"] * df["production/(production+imports)"]
    df["Estimated Supply Imported"] = df["Domestic supply quantity"] * df["imports/(production+imports)"]

    variables = [
        "Production", 
        "Import Quantity",
        "Export Quantity", 
        "Domestic supply quantity", 
        "Estimated Supply Produced", 
        "Estimated Supply Imported",
        "Food", 
        "Estimated Food Produced", 
        "Estimated Food Imported"
    ]
    df = df.rename(columns={var: f"{var} (1000 t)" for var in variables})
    df = pd.merge(df, df_pop, how = "left", on = "Year")

    units = ["kcal", "protein g", "fat g"]
    for unit in units:
        for variable in variables:
            df[f"{variable} (bln {unit})"] = df[f"{variable} (1000 t)"] * df[f"{unit}/kg"] / 1000
            df[f"{variable} ({unit}/capita/day)"] = df[f"{variable} (bln {unit})"] * 1e9 / df["Population"] / days_in_year

    columns = (
        index + 
        [f"{v} (1000 t)" for v in variables] + 
        [f"{v} (bln {u})" for u in units for v in variables] + 
        [f"{v} ({u}/capita/day)" for u in units for v in variables] + 
        [f"{unit}/kg" for unit in units] +
        ["food/supply", "(production+imports)/supply", "production/(production+imports)", "imports/(production+imports)"]
    )
    df = df[columns].sort_values(by=["Item Code", "Year"])

    df.to_csv(file, index=False, float_format="%.3f")


def average_country_data(country, years, force=False):

    os.makedirs(data_dir("clean", "country_data"), exist_ok=True)

    file = data_dir("clean", "country_data", f"fao_country_data;country={country};years={years[0]}-{years[-1]}.csv")
    if not force and file.exists():
        return 
    
    source_file = data_dir("clean", "country_data", f"fao_country_data;country={country}.csv")
    if not source_file.exists():
        return
    
    df = pd.read_csv(source_file)
    
    # Limit data range to specified years
    df = df[df["Year"].isin(years)]
    # Aggregate by element and item, average over years
    df = df.drop(columns="Year").groupby(by=["Item", "Item Code"]).mean().reset_index()
    df = df.sort_values(by="Domestic supply quantity (bln kcal)", ascending=False)
    if df.empty:
        return

    df.to_csv(file, index=False, float_format="%.3f")


def plot_top_products(country, years, variable="food", unit="kcal", per_capita=True, n_top=20, force=False, title=None):

    os.makedirs(plots_dir("country_data_plots"), exist_ok=True)

    file = plots_dir(
        "country_data_plots", 
        f"fao_country_data;country={country};unit={unit};variable={variable};years={years[0]}-{years[-1]}.png"
    )
    if not force and file.exists():
        return 

    source_file = data_dir("clean", "country_data", f"fao_country_data;country={country};years={years[0]}-{years[-1]}.csv")
    if not source_file.exists():
        print(f"No data for {country}")
        return
    
    df = pd.read_csv(source_file)

    if per_capita:
        unit = f"{unit}/capita/day"
    else:
        unit = f"bln {unit}"

    production_col = f"Estimated {variable.capitalize()} Produced ({unit})"
    imports_col = f"Estimated {variable.capitalize()} Imported ({unit})"
    total_col = f"{variable.capitalize()} ({unit})"
    df[total_col] = df[production_col].fillna(0) + df[imports_col].fillna(0)
    columns = ["Item", production_col, imports_col, total_col]
    df = df[columns]
    df = df[df[total_col] > 0]
    df = df.sort_values(by=total_col, ascending=False)

    # Keep the top n_top, aggregate the rest
    df = df.reset_index(drop=True)
    df["id"] = df.index
    df.loc[df.index >= n_top, ["id", "Item"]] = [n_top, "Others"]
    df = df.groupby(by="id").agg({
            production_col: "sum",
            imports_col: "sum",
            total_col: "sum", 
            "Item": "first"
        }).reset_index()

    # Plot
    sns.set_theme(style="whitegrid")
    sns.set_context("paper")
    plt.rcParams.update({"font.sans-serif": "Lato"})
    # sns.set_color_codes("pastel")
    # Initialize the matplotlib figure
    fig, ax = plt.subplots(figsize=(2.5, 3))

    # Imported - part will be hidden under produced hence we need total
    sns.set_color_codes("pastel")
    sns.barplot(
        data=df, 
        x=total_col, 
        y="Item",
        label="imported (est.)", 
        color="b"
    )

    # Produced
    sns.set_color_codes("muted")
    sns.barplot(
        data=df, 
        x=production_col, 
        y="Item",
        label="produced (est.)", 
        color="b"
    )

    sns.despine(left=True, bottom=True)
    handles, labels = ax.get_legend_handles_labels()    
    ax.legend(
        reversed(handles), reversed(labels), 
        ncol=2, loc="lower center", bbox_to_anchor=(0.1, -0.3), frameon=False, borderpad=0, borderaxespad=0
    )
    if title is None:
        title = f"{country}, {years[0]}-{years[-1]} average"
    ax.set(
        ylabel="", 
        xlabel=total_col, 
        title=title,
    )
    # if unit == "kcal/capita/day":
    #     ax.set_xlim([0, 750])


    fig.savefig(file, bbox_inches="tight", dpi=300)

    plt.close()


def process_production_yield_data(country, items_fao, items_usda, force=False):


    os.makedirs(data_dir("clean", "country_data", "production_yield"), exist_ok=True)

    file = data_dir("clean", "country_data", "production_yield", f"production_yield_data;country={country}.csv")
    if not force and file.exists():
        return 
    
    dfs = []
    # FAO
    source_file = data_dir("clean", "fao", f"fao_country_data_{country}.csv")
    if source_file.exists():
        df = pd.read_csv(source_file)
        df = df[df["Element Code"].isin([5510, 5419])]
        df = df[df["Item"].isin(items_fao)]
        df["Source"] = "FAO"
        df = df.rename(columns={"Element": "Variable"})
        df.loc[df["Variable"] == "Production", "Value"] /= 1000 # convert to 1000 t
        df.loc[df["Variable"] == "Yield", "Value"] /= 10000 # convert to t/ha
        df = df[["Year", "Item", "Variable", "Source", "Value"]]
        dfs.append(df)

    source_file = data_dir("clean", "country_data", f"fao_country_data;country={country}.csv")
    if source_file.exists():
        df = pd.read_csv(source_file)
        cols = ["Production", "Domestic supply quantity", "Import Quantity", "Export Quantity", "Food"]
        df = df[["Year"] + [f"{col} (bln kcal)" for col in cols]]
        df = df.rename(columns={f"{col} (bln kcal)": col for col in cols})
        df = df.groupby("Year").sum().reset_index()
        df = df.melt(id_vars="Year", var_name="Item", value_name="Value")
        df["Source"] = "FAO"
        df["Variable"] = "Food Supply"
        df = df[["Year", "Item", "Variable", "Source", "Value"]]
        dfs.append(df)

    # USDA
    source_file = data_dir("clean", "usda", f"usda_country_data_{country}.csv")
    if source_file.exists():
        df = pd.read_csv(source_file)
        df = df[df["Attribute_ID"].isin([28, 184])]
        df = df[df["Commodity_Description"].isin(items_usda)]
        df["Source"] = "USDA"
        df = df.rename(columns={
            "Market_Year": "Year",
            "Commodity_Description": "Item", 
            "Attribute_Description": "Variable"
        })
        df["Item"] = df["Item"].replace({
            item_usda: item_fao 
            for item_usda, item_fao in zip(items_usda, items_fao)
            if item_usda != "" and item_fao != ""
        })
        paddy_to_milled_conversion_factor = 0.7
        df.loc[
            (df["Item"] == "Rice, paddy") & (df["Variable"] == "Production"), 
            "Value"
        ] /= paddy_to_milled_conversion_factor
        df = df[["Year", "Item", "Variable", "Source", "Value"]]
        dfs.append(df)
    
    df = pd.concat(dfs)
    df = df.sort_values(["Item", "Variable", "Source", "Year"])

    # Detrend
    subdfs = []
    for (key, subdf) in df.groupby(by=["Item", "Variable", "Source"]):
        print("Fitting", key)
        subdf["Area"] = country
        subdf = detrend_timeseries(
            subdf,
            var_col="Value",
            year_col="Year",
            model="lowess",
            knot_interval=10,
        )
        subdf = subdf.drop(columns="Area")
        subdfs.append(subdf)
    df = pd.concat(subdfs)
    df.to_csv(file, index=False)



def plot_production_yield_data(variable, countries, items = None, force=False):

    os.makedirs(plots_dir("country_data_plots", "timeseries"), exist_ok=True)

    file1 = plots_dir("country_data_plots", "timeseries", f"{variable.lower()}_value_by_country.png")
    file2 = plots_dir("country_data_plots", "timeseries", f"{variable.lower()}_value_by_item.png")
    file3 = plots_dir("country_data_plots", "timeseries", f"{variable.lower()}_anomaly_by_country.png")
    file4 = plots_dir("country_data_plots", "timeseries", f"{variable.lower()}_anomaly_by_item.png")

    if not force and file1.exists() and file2.exists() and file3.exists() and file4.exists():
        return 
    
    dfs = []
    for country in countries:
        file = data_dir("clean", "country_data", "production_yield", f"production_yield_data;country={country}.csv")
        df = pd.read_csv(file)
        df["Country"] = country
        df = df[df["Variable"] == variable].drop(columns="Variable")
        df["Value Relative Anomaly"] = df["Value Anomaly"] / df["Value Trend"]
        df.columns = [col.replace("Value", variable) for col in df.columns]
        if items:
            df = df[df["Item"].isin(items)]
        df = df.melt(id_vars = ["Country", "Item", "Source", "Year"], var_name="Variable", value_name="Value")
        dfs.append(df)

    df = pd.concat(dfs).reset_index(drop=True)

    unit_dict = {"Yield": "t/ha", "Production": "1000 t", "Food Supply": "bln kcal"}

    #sns.set_theme(style="whitegrid")
    sns.set_context("paper")
    plt.rcParams.update({"font.sans-serif": "Lato"})
    sns.set_color_codes("pastel")


    def plot_grid(col, hue, var, unit):
        g = sns.relplot(
            data=df[df["Variable"] == var], 
            x="Year", 
            y="Value", 
            hue=hue, 
            style="Source",
            col=col,
            col_wrap=3,
            kind="line",
            markers=True, 
            aspect=1.2,
            height=2.5,
        )
        g.set_axis_labels(
            x_var="",
            y_var=f"{variable} ({unit})",
        )
        sns.despine()
        
        #ax.legend(bbox_to_anchor=(1.02, 1), loc="upper left", borderaxespad=0)
        for ax in g.axes:
            ax.xaxis.set_minor_locator(MultipleLocator(1))
            ax.xaxis.set_major_locator(MultipleLocator(5))
        fig = g.figure
        # fig.tight_layout()
        return fig

    df = df[df["Year"] >= 2000]
        
    dpi = 300
    # Value by country
    fig = plot_grid(col="Country", hue="Item", var=variable, unit=unit_dict[variable])
    fig.savefig(file1, dpi=dpi)
    plt.close()

    # Value by item
    fig = plot_grid(col="Item", hue="Country", var=variable, unit=unit_dict[variable])
    fig.savefig(file2, dpi=dpi)
    plt.close()

    # Anomaly by country
    fig = plot_grid(col="Country", hue="Item", var=f"{variable} Relative Anomaly", unit="anomaly")
    fig.savefig(file3, dpi=dpi)
    plt.close()

    # Anomaly by item
    fig = plot_grid(col="Item", hue="Country", var=f"{variable} Relative Anomaly", unit="anomaly")
    fig.savefig(file4, dpi=dpi)
    plt.close()



if __name__ == "__main__":

    country_names = {"BDI": "Burundi", "DJI": "Djibouti", "ERI": "Eritrea", "ETH": "Ethiopia", "KEN": "Kenya", "RWA": "Rwanda", "SDN": "Sudan", "SOM": "Somalia", "SSD": "South Sudan", "TZA": "Tanzania", "UGA": "Uganda"}

    countries = ["BDI", "DJI", "ERI", "ETH", "KEN", "RWA", "SDN", "SOM", "SSD", "TZA", "UGA"]
    years = [(2006, 2007, 2008), (2016, 2017, 2018)]

    items_fao = ["Wheat", "Maize", "Rice, paddy", "Millet", "Sorghum", "Cereals nes"]
    items_usda = ["Wheat", "Corn", "Rice, Milled", "Millet", "Sorghum", ""]

    for country in countries:
        process_country_data(country, force=False)
        process_production_yield_data(country, items_fao, items_usda, force=True)

    for country, years in product(countries, years):
        average_country_data(country, years, force=False)
        for variable, unit in product(["supply", "food"], ["kcal", "protein g", "fat g"]):
            title = f"{country_names[country]}, {years[0]}-{years[-1]} average"
            plot_top_products(country, years, variable=variable, unit=unit, title=title, n_top=7, force=False)

    countries = ["KEN", "UGA", "ETH", "TZA", "SOM"]
    items = items_fao[:5]
    for variable in ["Production", "Yield"]:
        plot_production_yield_data(variable, countries, items=items, force=True)

    for variable in ["Food Supply"]:
        plot_production_yield_data(variable, countries, force=True)




