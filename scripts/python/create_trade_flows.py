from textwrap import indent
import pandas as pd
import numpy as np
import os

from AgriculturalData.config import data_dir
from AgriculturalData.data_processing import impute_trade_flows_fao, aggregate_trade_flows


def create_trade_flows_fao(force=False):

    os.makedirs(data_dir("clean", "trade_flows"), exist_ok=True)

    crops = ["wheat", "rice", "maize"]

    for crop in crops:
        file = data_dir("clean", "trade_flows", f"{crop}_trade_flows_fao.csv")
        if file.exists() and not force:
            continue
        df = impute_trade_flows_fao(crop)
        df.to_csv(file, index=False)

    for crop in crops:
        df = pd.read_csv(data_dir("clean", "trade_flows", f"{crop}_trade_flows_fao.csv"))
        file = data_dir("clean", "trade_flows", f"{crop}_trade_flows_fao_agg.csv")
        if file.exists() and not force:
            continue
        df = aggregate_trade_flows(df, over_items=True)
        df.to_csv(file, index=False)
    

if __name__ == "__main__":

    create_trade_flows_fao()


