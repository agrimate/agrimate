import os
import pandas as pd

from AgriculturalData.config import data_dir


def generate_crop_calendar(crop, force=False):

    os.makedirs(data_dir("clean", "crop_calendars"), exist_ok=True)
    file = data_dir("clean", "crop_calendars", f"{crop}_crop_calendar.csv")
    if not force and file.exists():
        return

    # PROCESS SAGE DATA
    df = pd.read_csv(data_dir("clean", "sage", "crop_calendar_sage.csv"))
    df = df[df["Crop"] == crop.capitalize()]
    # Set the source of data
    df["Source"] = "SAGE Crop Calendar Dataset ID " + df["Data.ID"].astype(str)
    # Remove entries with "Exclude" qualifier
    df["Qualifier"] = df["Qualifier"].fillna("")
    df = df[~df["Qualifier"].str.contains("Exclude", case=False)]
    # Add qualifier to the crop name, which specifies e.g. mutltiple harvest periods
    df.loc[df["Qualifier"] != "", "Crop"] = df["Crop"] + "." + df["Qualifier"]
    # For countries with multiple locations and crop qualifiers estimate production share by harvested area share
    total_area = df.groupby("ISO3 Code")["harvested.area"].sum()
    total_area.name = "total.harvested.area"
    df = df.join(total_area, on="ISO3 Code")
    df["Country Share"] = df["harvested.area"] / df["total.harvested.area"]
    # For countries with one location and missing production share, the share can be unambiguously set to 1
    one_location = df.groupby("ISO3 Code").size().reset_index(name="count").query("count == 1")["ISO3 Code"]
    df.loc[df["ISO3 Code"].isin(one_location), "Country Share"] = df["Country Share"].fillna(1)
    # Drop entries with missing country share
    df = df[~df["Country Share"].isna()]
    # Drop entries with missing harvest dates
    df = df[~df["Harvest.start"].isna() & ~df["Harvest.end"].isna()]
    columns = ["ISO3 Code", "Location", "Crop", "Harvest.start", "Harvest.end", "Country Share", "Source"]
    df = df[columns]

    # UPDATE / EXTEND SAGE

    # Load extra datasets
    df_extra = pd.read_excel(
        data_dir("clean", "crop_calendars", "ExtraCropCalendars.xlsx"),
        sheet_name=crop, 
        usecols=columns
    )
    df_extra_usda = pd.read_csv(
        data_dir("raw", "usda", "crop_calendar_USA.csv"),
        usecols=[col for col in columns if col != "Source"]
    )
    df_extra_usda["Source"] = "USDA Usual Planting and Harvesting Dates + Crop Production Summary"
    df_extra_usda = df_extra_usda[df_extra_usda["Crop"].str.contains(crop.capitalize())]
    df_extra = pd.concat([df_extra, df_extra_usda])
    # Drop SAGE entries for updated countries
    updated_countries = df_extra["ISO3 Code"].unique()
    df = df[~df["ISO3 Code"].isin(updated_countries)]
    # Merge two datasets
    df = pd.concat([df, df_extra])

    # Assume crop calendars equal for some neighboring / ancestor countries
    if crop == "wheat":
        assumed_calendars = {
            "COG": "AGO", "CMR": "AGO", "COD": "ZMB", "BDI": "KEN", "UGA": "KEN", "RWA": "KEN", "SOM": "ERI",
            "TCD": "NGA", "MLI": "NGA", "NER": "NGA", "SLE": "NGA", "MRT": "NGA",
            "MYS": "MMR", "THA": "MMR", "BTN": "NPL",
            "OMN": "YEM", "QAT": "SAU", "KWT": "SAU", "ARE": "SAU", "PSE": "ISR",
            "MNE": "SRB",
            "HND": "GTM", "TTO": "COL", "VEN": "COL",
            "CSK": "CZE", "SCG": "SRB", "YMD": "YEM", "BLX": "BEL",
            "TWN": ("CHN", "China (North China Plain)"),
            "MNG": ("CHN", "China (north)"),
            "NZL": ("AUS", "New South Wales & Victoria"),
            "NCL": ("AUS", "New South Wales & Victoria"),
        }
    elif crop == "rice":
        assumed_calendars = {
            "TKM": "KAZ", "KGZ": "KAZ", "UZB": "KAZ", "TJK": "KAZ", "UGA": "KEN", "RWA": "KEN", "ETH": "KEN",
            "ZMB": "ZWE", "MOZ": "ZWE", "SDN": "NER", "HUN": "EU27", "YUG": "EU27", "ROU": "EU27", "BGR": "EU27", 
            "AZE": "TUR", "MAR": "EGY", "JAM": "GTM", "HND": "GTM", "BTN": "NPL", "PRT": "EU27", "ALB": "EU27",
            "TWN": "PHL", "FRA": "EU27", "REU": "MDG", "MUS": "MDG", "PRI": "DOM", "PNG": "TLS", "BDI": "KEN",
            "BLZ": "GTM", "COM": "MDG", "GAB": "CMR", "COG": "CMR", "GRC": "EU27",
            "BRN": ("MYS", "Malaysia (Sarawak prov.)"),
            "FJI": ("AUS", "New South Wales"), "SLB": ("AUS", "New South Wales"), "FSM": ("AUS", "New South Wales")
        }
    elif crop == "maize":
        assumed_calendars = {
            "AZE": "TUR", "YEM": "SAU", "JPN": "KOR", "TUN": "EGY", "JOR": "EGY", "GUY": "VEN", "SCG": "YUG", 
            "SRB": "YUG", "BIH": "HUN", "KAZ": "KGZ", "SVK": "HUN", "SVN": "EU27", "HRV": "EU27", "TJK": "KGZ", 
            "ISR": "JOR", "LBN": "JOR", "CZE": "EU27", "CSK": "EU27", "CHE": "EU27", "SSD": "SDN", "LAO": "THA", 
            "POL": "EU27", "DZA": "EGY", "BTN": "NPL", "DEU": "EU27", "PRT": "EU27", "BEL": "EU27", "SWE": "EU27",
            "DNK": "EU27", "AUT": "EU27", "NLD": "EU27", "LTU": "EU27", "GRC": "EU27", "LUX": "EU27", "BLX": "EU27",
            "GBR": "EU27",
            "BLR": "UKR", "BHS": "CUB", "COM": "MDG", "REU": "MDG", "LBY": "DZA", "MUS": "MDG", "BRB": "TTO", 
            "VCT": "TTO", "MNE": "YUG", "MDV": "MDG", "HND": "GTM", "PNG": "TLS", "ATG": "TTO", "BLZ": "GTM", 
            "ARE": "GEO", "DJI": "ERI", "STP": "GAB", "PRI": "DOM", "KWT": "IRQ", "MYS": "THA", "TWN": "PHL", 
            "OMN": "SAU", "JAM": "CUB", "GRD": "TTO",  "QAT": "SAU", "DMA": "TTO", "ARM": "GEO",
            "FJI": ("AUS", "New South Wales"), "NCL": ("AUS", "New South Wales"), "FSM": ("AUS", "New South Wales"),
            "NZL": ("AUS", "New South Wales"), "NRU": ("AUS", "New South Wales"), "VUT": ("AUS", "New South Wales"),
            "BGD": ("IND", "Orissa"), 
        }
    elif crop == "soybeans":
        assumed_calendars = {
            "CHL": "ARG", "HRV": "EU27", "FRA": "EU27", "ESP": "EU27", "HUN": "EU27", "DEU": "EU27", "CHE": "EU27",
            "GRC": "EU27", "AUT": "EU27", "ROU": "EU27", "CSK": "EU27", "PRT": "EU27", "SYR": "IRN", "GTM": "MEX", 
            "ECU": "COL", "SCG": "BIH", "NIC": "COL", "ZMB": "TZA", "UGA": "TZA", "YUG": "ROU", "SRB": "EU27"
        }
    else:
        assumed_calendars = {}
    # Create extra entries based on assumed calendars dictionary
    # df = df.set_index("ISO3 Code")
    for area1, area2 in assumed_calendars.items():
        assert not area1 in df["ISO3 Code"]
        if not isinstance(area2, tuple):
            df_area = df[df["ISO3 Code"] == area2].copy()
            df_area["Source"] = f"assumed same as {area2}"
        else:
            area2, location2 = area2
            df_area = df[(df["ISO3 Code"] == area2) & (df["Location"] == location2)].copy()
            df_area["Country Share"] /= df_area["Country Share"].sum()
            df_area["Source"] = f"assumed same as {area2}, {location2}"
        df_area["ISO3 Code"] = area1
        df_area["Location"] = pd.NA
        df = pd.concat([df, df_area])
  
    # Fill in missing country names
    country_names = pd.read_csv(data_dir("clean", "fao", "area_codes_fao.csv"), usecols=["ISO3 Code", "Area"])
    country_names = country_names.groupby(by=["ISO3 Code"]).first().reset_index()
    df = pd.merge(df, country_names, on="ISO3 Code", how="left")
    df["Location"] = df["Location"].fillna(df["Area"])
    df = df.drop(columns="Area")

    # FINALIZE
    # Round Country Share and check normalization
    df["Country Share"] = df["Country Share"].round(6)
    df_agg = df.groupby(by="ISO3 Code").agg({"Country Share": "sum"}).reset_index()
    assert len(df_agg[(df_agg["Country Share"] - 1).abs() > 1e-5]) == 0
    # Sort by ISO codes and location names
    df = df.sort_values(by=["ISO3 Code", "Location"])
    # Rename day columns and cast them to int
    df = df.rename(columns={"Harvest.start": "Harvest Start", "Harvest.end": "Harvest End"})
    for col in ["Harvest Start", "Harvest End"]:
        df[col] = df[col].astype(int)
    # Save
    df.to_csv(file, index=False)


def generate_crop_calendars_all(force=False):
    for crop in ["wheat", "rice", "maize"]:
        generate_crop_calendar(crop, force=force)


if __name__ == "__main__":

    generate_crop_calendars_all()
