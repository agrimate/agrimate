import os
import pandas as pd

from AgriculturalData.config import data_dir
from AgriculturalData.data_processing import aggregate_trade_flows
from AgriculturalData.trade_harmonization import harmonize_trade_data, summarize_harmonized_data, verify_harmonization
from AgriculturalData.rebalance import rebalance_after_harmonization, rebalance_stock_variation, verify_food_balance


def create_baseline_data(crop, year_start, year_end, force=False, force_harmonization=None):

    os.makedirs(data_dir("clean", "baseline_data"), exist_ok=True)
    os.makedirs(data_dir("cache", "baseline_data"), exist_ok=True)

    suffix = f"_harmonized;baseline={year_start}-{year_end};crop={crop}"
    file_fb_h = data_dir("cache", "baseline_data", f"food_balance{suffix}.csv")
    file_t_h = data_dir("cache", "baseline_data", f"trade_flows{suffix}.csv")
    force_harmonization = force if force_harmonization is None else force_harmonization 
    if force_harmonization or not file_fb_h.exists() or not file_t_h.exists():
        print(f"CREATING BASELINE DATA (crop: {crop}, baseline: {year_start}-{year_end})")

        # Load food balance data and average over years
        df_fb = pd.read_csv(data_dir("clean", "food_balances", f"{crop}_food_balance_fao.csv"))
        df_fb = df_fb[(year_start <= df_fb["Year"]) & (df_fb["Year"] <= year_end)].drop(columns="Year")
        df_fb["Area"] = df_fb["ISO3 Code"]
        df_fb = df_fb.drop(columns=["Area Code", "ISO3 Code"])
        df_fb = df_fb.groupby(by="Area").mean().reset_index()

        # Load trade flow data and average over years
        df_t = pd.read_csv(data_dir("clean", "trade_flows", f"{crop}_trade_flows_fao.csv"))
        df_t = df_t[(year_start <= df_t["Year"]) & (df_t["Year"] <= year_end)].drop(columns="Year")
        df_t = aggregate_trade_flows(df_t, over_items=True, over_years=True)
        for role in ["Origin", "Destination"]:
            df_t[role] = df_t[f"{role} ISO3 Code"]
            df_t = df_t.drop(columns=[f"{role} Code", f"{role} ISO3 Code"])
        df_t = df_t.groupby(by=["Origin", "Destination"]).sum().reset_index()

        # Remove entries where Origin == Destination
        df_t = df_t[df_t["Origin"] != df_t["Destination"]] 

        # Harmonize trade
        df_fb, df_t = harmonize_trade_data(df_fb, df_t)
        
        df_fb = df_fb.round(3)
        df_t = df_t.round(3)
        summarize_harmonized_data(df_t)
        df_fb.to_csv(file_fb_h, index=False)
        df_t.to_csv(file_t_h, index=False)
        
        verify_harmonization(df_fb, df_t)

    suffix = f";baseline={year_start}-{year_end};crop={crop}"
    file_fb = data_dir("clean", "baseline_data", f"food_balance{suffix}.csv")
    file_t = data_dir("clean", "baseline_data", f"trade_flows{suffix}.csv")
    if force or not file_fb.exists() or not file_t.exists():
        df_fb = pd.read_csv(file_fb_h)
        df_t = pd.read_csv(file_t_h)
        # Rebalance
        df_fb = rebalance_after_harmonization(df_fb)
        verify_food_balance(df_fb, rebalanced=True)

        df_fb, df_t = rebalance_stock_variation(df_fb, df_t)

        df_fb = df_fb.round(3)
        df_t = df_t.round(3)

        # Save
        df_fb.to_csv(file_fb, index=False)
        df_t.to_csv(file_t, index=False)
        
        verify_food_balance(df_fb, rebalanced=True)



def create_baseline_data_all(force=False, force_harmonization=None):

    baseline_periods = [(2004, 2006), (2007, 2009), (2010, 2012), (2013, 2015), (2016, 2018)]
    crops = ["wheat", "maize", "rice"]

    for year_start, year_end in baseline_periods:
        for crop in crops:
            create_baseline_data(crop, year_start, year_end, force=force, force_harmonization=force_harmonization)


if __name__ == "__main__":

    # create_baseline_data_all(force=True, force_harmonization=False)
    create_baseline_data_all()

