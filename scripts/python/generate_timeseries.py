import numpy as np
import os
import pandas as pd

from AgriculturalData.config import data_dir
from AgriculturalData.timeseries import generate_daily_production_timeseries, generate_timeseries_fao, generate_timeseries_usda



def generate_timeseries_all_fao(force=False):

    os.makedirs(data_dir("clean", "timeseries"), exist_ok=True)

    crops = ["wheat", "maize", "rice"]
    for crop in crops:

        file = data_dir("clean", "timeseries", f"production;crop={crop};source=FAO.csv")
        if force or not file.exists():
            df = generate_timeseries_fao(crop, "Production")
            df.to_csv(file, index=False)

        file = data_dir("clean", "timeseries", f"domestic_supply;crop={crop};source=FAO.csv")
        if force or not file.exists():
            df = generate_timeseries_fao(crop, "Domestic supply quantity")
            df.to_csv(file, index=False)

        file = data_dir("clean", "timeseries", f"stocks;crop={crop};source=FAO.csv")
        if force or not file.exists():
            df = generate_timeseries_fao(crop, "Stock Variation", detrend=False)
            df.to_csv(file, index=False)

        file = data_dir("clean", "timeseries", f"daily_production;crop={crop};source=FAO.csv")
        if force or not file.exists():
            df = generate_daily_production_timeseries(crop, source="FAO")
            df.to_csv(file, index=False)


def generate_timeseries_all_usda(force=False):

    def convert_rice_milled_to_paddy(df, variable):
        conversion_factor = 0.7
        columns = [col for col in df.columns if variable in col]
        for col in columns:
            df[col] /= conversion_factor
        return df

    os.makedirs(data_dir("clean", "timeseries"), exist_ok=True)

    for crop in ["wheat", "rice", "maize"]:

        year_min = 2000

        file = data_dir("clean", "timeseries", f"production;crop={crop};source=USDA.csv")
        if force or not file.exists():
            filenames = ["psd_alldata.csv", "psd_eu_area_prod.csv"]
            dfs = []
            for filename in filenames:
                df = generate_timeseries_usda(crop, "Production", filename=filename, year_min=year_min-1)
                dfs.append(df)
            df = pd.concat(dfs).sort_values(["Area", "Year"])
            df = df[df["Area"] != "E4"]
            if crop == "rice":
                df = convert_rice_milled_to_paddy(df, "Production")
            df.to_csv(file, index=False)

                
        file = data_dir("clean", "timeseries", f"domestic_supply;crop={crop};source=USDA.csv")
        if force or not file.exists():
            df = generate_timeseries_usda(crop, "Domestic Consumption", filename="psd_alldata.csv", year_min=year_min-1)
            df.columns = [col.replace("Consumption", "supply quantity") for col in df.columns]
            df.loc[df["Area"] == "E4", "Area"] = "EU-27"
            if crop == "rice":
                df = convert_rice_milled_to_paddy(df, "Domestic supply quantity")
            df.to_csv(file, index=False)

        file = data_dir("clean", "timeseries", f"stocks;crop={crop};source=USDA.csv")
        if force or not file.exists():
            df = generate_timeseries_usda(crop, "Ending Stocks", filename="psd_alldata.csv", year_min=year_min-1, detrend=False)
            df.loc[df["Area"] == "E4", "Area"] = "EU-27"
            df["Ending Stocks"] = df["Ending Stocks"].replace({0: np.nan})
            if crop == "rice":
                df = convert_rice_milled_to_paddy(df, "Ending Stocks")
            df["Stock Variation"] = df.groupby(by=["Area"], sort=False)["Ending Stocks"].transform("diff")
            df.to_csv(file, index=False)

        file = data_dir("clean", "timeseries", f"daily_production;crop={crop};source=USDA.csv")
        if force or not file.exists():
            df = generate_daily_production_timeseries(crop, source="USDA")
            df.to_csv(file, index=False)



if __name__ == "__main__":

    generate_timeseries_all_fao()
    generate_timeseries_all_usda()

