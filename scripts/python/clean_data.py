import os
import pandas as pd
from icecream import ic

from AgriculturalData.config import data_dir
from AgriculturalData.data_cleaning import clean_fao, clean_usda_marketing_year_data
from AgriculturalData.data_cleaning import remove_aggregate_areas, reverse_stock_variation_sign, convert_rice_milled_to_paddy

from AgriculturalData.item_groups import item_groups_fao, item_group_codes_fao


def clean_all_fao(force=False):
    
    os.makedirs(data_dir("clean", "fao"), exist_ok=True)

    item_group_names = ["wheat", "maize", "rice"]

    years = list(range(1992, 2022))



    def clean_area_codes(df):
        df = df.rename(columns={"Country": "Area", "Country Code": "Area Code"})
        df = df.groupby(by="Area Code").first().reset_index().sort_values(by="Area Code")
        # Change some ISO3 Codes
        df = df.set_index("Area Code")
        df.loc[41, "ISO3 Code"] = "CHN" # China, mainland
        df.loc[206, "ISO3 Code"] = "SDN" # Sudan (former)
        df.loc[62, "ISO3 Code"] = "ETH" # Ethiopia PDR
        df.loc[51, "ISO3 Code"] = "CSK" # Czechoslovakia
        df.loc[15, "ISO3 Code"] = "BLX" # Belgia-Luxembourg
        df = df.reset_index()
        return df


    for group_name in item_group_names:

        # Food Balance Sheets
        file = data_dir("clean", "fao", f"{group_name}_food_balance.csv")
        if force or not file.exists():
            df1 = clean_fao(
                filename="FoodBalanceSheetsHistoric_E_All_Data_(Normalized).csv", 
                years=[y for y in years if y < 2014],
                item_codes=item_group_codes_fao[group_name],
                elements=["Production", "Import Quantity", "Export Quantity", "Domestic supply quantity", "Stock Variation"],
                columns=["Year", "Area", "Area Code", "Item", "Item Code", "Element", "Element Code", "Value"],
                extra_funcs=[remove_aggregate_areas, reverse_stock_variation_sign, convert_rice_milled_to_paddy],
            )
            df2 = clean_fao(
                filename="FoodBalanceSheets_E_All_Data_(Normalized).csv", 
                years=[y for y in years if y >= 2014],
                item_codes=item_group_codes_fao[group_name],
                elements=["Production", "Import Quantity", "Export Quantity", "Domestic supply quantity", "Stock Variation"],
                columns=["Year", "Area", "Area Code", "Item", "Item Code", "Element", "Element Code", "Value"],
                extra_funcs=[remove_aggregate_areas],
            )
            df = pd.concat([df1, df2])
            df.to_csv(file, index=False)
        
        # Production and Trade Data
        for filename, elements, suffix in zip(
            [
                "Production_Crops_Livestock_E_All_Data", 
                "Value_of_Production_E_All_Data", 
                "Trade_CropsLivestock_E_All_Data"
            ],
            [
                ["Production"], 
                ["Gross Production Value (current thousand US$)"], 
                ["Export Quantity", "Import Quantity", "Export Value", "Import Value"]
            ],
            [
                "production", 
                "production_value", 
                "trade"
            ]
        ):
            file = data_dir("clean", "fao", f"{group_name}_{suffix}.csv")
            if force or not file.exists():
                df = clean_fao(
                    filename=filename + "_(Normalized)" + ".csv", 
                    years=years,
                    item_codes=list(item_groups_fao[group_name].keys()),
                    elements=elements,
                    columns=["Year", "Area", "Area Code", "Item", "Item Code", "Element", "Element Code", "Value"],
                    extra_funcs=[remove_aggregate_areas],
                )
                df.to_csv(file, index=False)
        
        # Detailed Trade Data
        file = data_dir("clean", "fao", f"{group_name}_trade_detailed.csv")
        if force or not file.exists():
            df = clean_fao(
                filename="Trade_DetailedTradeMatrix_E_All_Data_(Normalized).csv", 
                years=years,
                item_codes=list(item_groups_fao[group_name].keys()),
                elements=["Import Quantity", "Export Quantity"],
                columns=["Year", "Reporter Countries", "Reporter Country Code", "Partner Countries", "Partner Country Code", 
                         "Item", "Item Code", "Element", "Element Code", "Value"],
                extra_funcs=[remove_aggregate_areas],
            )
            df.to_csv(file, index=False)


    # Country Code to ISO
    file = data_dir("clean", "fao", "area_codes_fao.csv")
    if force or not file.exists():
        df = clean_fao(
            filename="FAOSTAT_Country_Region.csv",
            encoding='utf-8-sig',
            extra_funcs=[clean_area_codes],
        )
        df.to_csv(file, index=False)



def clean_sage(force=False):

    os.makedirs(data_dir("clean", "sage"), exist_ok=True)

    file = data_dir("clean", "sage", "crop_calendar_sage.csv")
    if not force and file.exists():
        return 

    # Read SAGE dataset
    columns = ["Data.ID", "Location", "location.code", "Level", "Nation.code", "State.code", "County.code",
               "Crop", "Qualifier", "Crop.name.in.original.data",
               "Harvest.start", "Harvest.end", "harvested.area"]
    df = pd.read_csv(data_dir("raw", "sage", "All_data_with_climate.csv"), na_values=["NA"], usecols=columns)

    # Attribute ISO3 Codes to Nation.code values
    codes = df.loc[df["Level"] == "N", ["Location", "Nation.code"]].drop_duplicates()
    area_codes = pd.read_csv(data_dir("clean", "fao", "area_codes_fao.csv"), usecols=["Area", "ISO3 Code"])
    # ic(codes[codes["Location"].isna()])
    codes = pd.merge(codes, area_codes, left_on="Location", right_on="Area", how="left").drop(columns="Area")
    # ic(codes[codes["ISO3 Code"].isna()])
    # Fill in missing values manually
    codes = codes.set_index("Nation.code")
    codes.loc[201, "ISO3 Code"] = "BOL"
    codes.loc[122, "ISO3 Code"] = "CPV"
    codes.loc[173, "ISO3 Code"] = "COD"
    codes.loc[52, "ISO3 Code"] = "PRK"
    codes.loc[196, "ISO3 Code"] = "TLS"
    codes.loc[172, "ISO3 Code"] = "GUF"
    codes.loc[139, "ISO3 Code"] = "GMB"
    codes.loc[63, "ISO3 Code"] = "IRN"
    codes.loc[159, "ISO3 Code"] = "CIV"
    codes.loc[103, "ISO3 Code"] = "LAO"
    codes.loc[57, "ISO3 Code"] = "MKD"
    codes.loc[35, "ISO3 Code"] = "MDA"
    codes.loc[3, "ISO3 Code"] = "RUS"
    codes.loc[64, "ISO3 Code"] = "KOR"
    codes.loc[229, "ISO3 Code"] = "SWZ"
    codes.loc[68, "ISO3 Code"] = "SYR"
    codes.loc[187, "ISO3 Code"] = "TZA"
    codes.loc[154, "ISO3 Code"] = "TTO"
    codes.loc[152, "ISO3 Code"] = "VEN"
    codes.loc[11, "ISO3 Code"] = "GBR"
    codes.loc[101, "ISO3 Code"] = "VNM"
    codes.loc[2, "ISO3 Code"] = "CAN"
    codes.loc[5, "ISO3 Code"] = "USA"
    codes.loc[167, "ISO3 Code"] = "MYS"
    codes.loc[177, "ISO3 Code"] = "UGA"
    codes = codes.reset_index().drop(columns="Location")
    codes = codes.drop_duplicates().dropna()
    # Join with the original dataset
    df = pd.merge(df, codes, on="Nation.code", how="left")
    # Attribute codes to YUG and CSK
    df.loc[df["Location"] == "Yugoslavia (Former)", "ISO3 Code"] = "YUG"
    df.loc[df["Location"] == "Czechoslovakia", "ISO3 Code"] = "CSK"
    df.to_csv(file, index=False)


def clean_usda_all(force=False):

    os.makedirs(data_dir("clean", "usda"), exist_ok=True)

    for crop in ["wheat", "rice", "maize"]:
        file = data_dir("clean", "usda", f"{crop}_marketing_years.csv")
        if not force and file.exists():
            continue
        df = clean_usda_marketing_year_data(crop)
        df = df[~df["USDA Code"].isin(["S8", "GM"])]
        df.to_csv(file, index=False)
 


if __name__ == "__main__":

    clean_all_fao()
    clean_sage()
    clean_usda_all()