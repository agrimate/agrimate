from AgriculturalData.config import data_dir
from AgriculturalData.downloads import download, download_und_unzip


def download_food_expenditure_data(force=False):

    # March 2022 data
    # https://www.ers.usda.gov/topics/international-markets-u-s-trade/international-consumer-and-food-industry-trends/#data
    download(
        url="https://www.ers.usda.gov/media/e2pbwgyg/2015-2020-food-spending_update-july-2021.xlsx",
        savedir=data_dir("raw", "usda"),
        filename="food_spending.xlsx",
        force=force,
    )

def download_export_value_data(force=False):

    # March 2022 data
    # https://data.worldbank.org/indicator/TX.VAL.MRCH.CD.WT

    download_und_unzip(
        url="https://api.worldbank.org/v2/en/indicator/TX.VAL.MRCH.CD.WT?downloadformat=csv",
        savedir=data_dir("raw", "world_bank"),
        filename="API_TX.VAL.MRCH.CD.WT_DS2_en_csv_v2_3731336.csv",
        force=force,
        rename=False,
    )

def download_income_group_data(force=False):

    # https://datahelpdesk.worldbank.org/knowledgebase/articles/906519-world-bank-country-and-lending-groups

    download(
        url="http://databank.worldbank.org/data/download/site-content/OGHIST.xlsx",
        savedir=data_dir("raw", "world_bank"),
        filename="OGHIST.xlsx",
        force=force,
    )

def download_budget_data_all(force=False):
    download_food_expenditure_data(force)
    download_export_value_data(force)
    download_income_group_data(force)
    

if __name__ == "__main__":

    download_budget_data_all()

