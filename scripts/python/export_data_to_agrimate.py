import pandas as pd
import numpy as np
import shutil

from AgriculturalData.config import data_dir


def export_food_balance_data(file, destination, force=True):
    if not force and destination.exists():
        return
    df = pd.read_csv(file)
    columns = [
        "Area", 
        "Production (rebalanced)", "Domestic supply quantity (rebalanced)", 
        "Export Quantity (harmonized) (rebalanced)", "Export Quantity (non-harmonized) (rebalanced)",
        "Import Quantity (harmonized) (rebalanced)", "Import Quantity (non-harmonized) (rebalanced)",
    ]
    df = df[columns]
    df.columns = [
        "Area", 
        "Production", "Consumption", 
        "Exports (harmonized)", "Exports (non-harmonized)",
        "Imports (harmonized)", "Imports (non-harmonized)",
    ]
    df = df.round(1)
    df = df[(df[df.columns[1:]] > 0).any(axis=1)]
    df.to_csv(destination, index=False)


def export_trade_flow_data(file, destination, force=True):
    if not force and destination.exists():
        return
    df = pd.read_csv(file)
    columns = ["Origin", "Destination", "Trade Flow (rebalanced)"] 
    df = df[columns]
    df.columns = ["Origin", "Destination", "Trade Flow"]
    df = df.round(1)
    df = df[df["Trade Flow"] > 0]
    df.to_csv(destination, index=False)


def export_baseline_data_all(force=False):

    baseline_data_dir = lambda *f: data_dir("clean", "baseline_data", *f)
    agrimate_input_dir = lambda *f: data_dir("agrimate_input", *f)

    for file in baseline_data_dir().iterdir():
        destination = agrimate_input_dir(file.name)
        if file.name.startswith("food_balance"):
            export_food_balance_data(file, destination, force=force)
        if file.name.startswith("trade_flows"):
            export_trade_flow_data(file, destination, force=force)


def export_harvest_timeseries(file, destination_anomaly, destination_trend, force=False):
    if not force and destination_anomaly.exists() and destination_trend.exists():
        return
    df = pd.read_csv(file)
    # Restrict years
    df = df[df["Year"] > 1999]
    # Remove areas which have no production
    areas_no_prod = df.groupby(by="Area").agg({"Daily Production Trend": "sum"}).reset_index()
    areas_no_prod = areas_no_prod.loc[areas_no_prod["Daily Production Trend"] == 0, "Area"].tolist()
    df = df[~df["Area"].isin(areas_no_prod)]
    # Create trend and anomaly datasets
    for var, dest in zip(["Daily Production Trend", "Daily Production Anomaly"], [destination_trend, destination_anomaly]):
        df_var = df.pivot(index=["Year", "Day of Year"], columns="Area", values=var).fillna(0).reset_index()
        df_var["Day"] = df_var["Year"].astype(str) + "-" + df_var["Day of Year"].astype(str)
        df_var = df_var.drop(columns=["Year", "Day of Year"]).set_index("Day")
        df_var = df_var.transpose().reset_index()
        df_var = df_var.round(5)
        df_var.to_csv(dest, index=False)



def export_harvest_timeseries_all(force=False):

    timeseries_dir = lambda *f: data_dir("clean", "timeseries", *f)
    agrimate_input_dir = lambda *f: data_dir("agrimate_input", *f)

    for file in timeseries_dir().iterdir():
        if file.name.startswith("daily_production"):
            suffix = ";".join(file.name.split(';')[1:])
            destination_anomaly = agrimate_input_dir(f"harvest_anomalies;{suffix}")
            destination_trend = agrimate_input_dir(f"harvest_trends;{suffix}")
            export_harvest_timeseries(file, destination_anomaly, destination_trend, force=force)


def export_harvest_distributions_all(force=False):

    harvest_distributions_dir = lambda *f: data_dir("clean", "harvest_distributions", *f)
    agrimate_input_dir = lambda *f: data_dir("agrimate_input", *f)

    for file in harvest_distributions_dir().iterdir():
        if file.name.startswith("harvest_distributions"):
            destination = agrimate_input_dir(file.name)
            if force or not destination.exists():
                shutil.copy(file, destination)



def export_parameter_data_all(force=False):

    baseline_periods = [(2004, 2006), (2007, 2009), (2010, 2012), (2013, 2015), (2016, 2018)]

    for baseline_start, baseline_end in baseline_periods:
        for crop in ["wheat", "maize", "rice"]:

            file = data_dir(
                "agrimate_input", 
                f"parameters;baseline={baseline_start}-{baseline_end};crop={crop};source=empirical.csv"
            )
            if not force and file.exists():
                return

            df = pd.read_csv(
                data_dir(
                    "agrimate_input", 
                    f"food_balance;baseline={baseline_start}-{baseline_end};crop={crop}.csv"
                ), 
                usecols=["Area"]
            )

            # Stock-to-use ratio
            df_s = pd.read_csv(data_dir("clean", "timeseries", f"stocks;crop={crop};source=USDA.csv"))
            df_c = pd.read_csv(data_dir("clean", "timeseries", f"domestic_supply;crop={crop};source=USDA.csv"))

            df_s = pd.merge(df_s, df_c, on=["Area", "Year"], how="left")
            df_s["STU"] = df_s["Ending Stocks"] / df_s["Domestic supply quantity"]

            df_s = df_s[(baseline_start <= df_s["Year"]) & (df_s["Year"] <= baseline_start)]
            df_s = df_s.groupby("Area").agg({"STU": np.mean}).reset_index()

            # EU-27 countries
            eu_areas = [
                "AUT", "BEL", "BGR", "CYP", "CZE", "DEU", "DNK", "ESP", "EST", 
                "FIN", "FRA", "GRC", "HRV", "HUN", "IRL", "ITA", "LTU", "LUX", 
                "LVA", "MLT", "NLD", "POL", "PRT", "ROU", "SVK", "SVN", "SWE"
            ]
            df_eu = pd.DataFrame({
                "Area": eu_areas, 
                "STU": [df_s.loc[df_s["Area"] == "EU-27", "STU"].iloc[0]] * len(eu_areas)
            })
            df_s = pd.concat([df_s, df_eu])

            df = pd.merge(df, df_s, on="Area", how="left")

            # Purchasing budget (A_d)
            df_d = pd.read_csv(data_dir("clean", "budget", f"budget;crop={crop}.csv"))
            mid_year = (baseline_start + baseline_end) // 2
            df_d = df_d.loc[df_d["Year"] == mid_year, ["ISO3 Code", "A_d (3-year window)"]]
            df_d = df_d.rename(columns={"A_d (3-year window)": "A_d"})
            df_d["A_d"] = np.maximum(0.0001, np.minimum(df_d["A_d"], 1.))

            df = pd.merge(df, df_d.rename(columns={"ISO3 Code": "Area"}), on="Area", how="left")

            # Consumption budget (A_c)
            df_c = pd.read_csv(data_dir("clean", "budget", f"food_spending.csv"))
            # Average over 2016-2018
            df_c = df_c[(2016 <= df_c["Year"]) & (df_c["Year"] <= 2018)]
            df_c = df_c.groupby("ISO3 Code").agg({"Food Spending Share": np.mean}).reset_index()
            df_c = df_c.rename(columns={"Food Spending Share": "A_c"})

            df = pd.merge(df, df_c.rename(columns={"ISO3 Code": "Area"}), on="Area", how="left")

            df.to_csv(file, index=False, float_format='%.4f')



def export_policy_data_all(force=False):

    for crop in ["wheat", "maize", "rice"]:

        source = "2007-2011"
        file = data_dir("agrimate_input", f"export_restrictions;crop={crop};source={source}.csv")
        if not force and file.exists():
            return

        df = pd.read_csv(data_dir("clean", "policies", f"{crop}_policies.csv"))

        df = df.rename(columns={
            "Country Code": "Exporter", 
            "Start": "From",
            "End": "To",
            "Policy Impact": "Value",
        })
        df = df[["Exporter", "From", "To", "Value"]]

        df.to_csv(file, index=False)




if __name__ == "__main__":

    export_baseline_data_all()
    export_harvest_distributions_all()
    export_harvest_timeseries_all()
    export_parameter_data_all(force=True)
    export_policy_data_all()

