from AgriculturalData.config import data_dir
from AgriculturalData.downloads import download, download_und_unzip


def download_all_fao(force=False):

    fao_url = r"https://fenixservices.fao.org/faostat/static/bulkdownloads/"

    fao_filenames = [    
        "FoodBalanceSheets_E_All_Data",
        "FoodBalanceSheetsHistoric_E_All_Data",
        "SUA_Crops_Livestock_E_All_Data",
        "Production_Crops_Livestock_E_All_Data",
        "Trade_CropsLivestock_E_All_Data",
        "Trade_DetailedTradeMatrix_E_All_Data",
        "Value_of_Production_E_All_Data",
        "Prices_E_All_Data",
        "ConsumerPriceIndices_E_All_Data",
        "Food_Security_Data_E_All_Data",
    ]
    fao_filenames = [filename + "_(Normalized)" for filename in fao_filenames]

    for filename in fao_filenames:
        download_und_unzip(
            url=fao_url + filename + ".zip",
            savedir=data_dir("raw", "fao"),
            filename=filename + ".csv",
            force=force,
        )

    # MANUAL DOWNLOAD
    # https://www.fao.org/faostat/en/#definitions
    # "FAOSTAT_Country_Region.csv" 
    # "FAOSTAT_Country_Group.csv"
    # "FAOSTAT_Items.csv"

    # EXTRA DATA SOURCES
    # Technical Conversion Factors for Agricultural Commodities
    # https://www.fao.org/fileadmin/templates/ess/documents/methodology/tcf.pdf
    # Linking the FAO commodity list to the main international classifications 
    # https://www.fao.org/waicent/faoinfo/economic/faodef/annexe.htm


def download_all_usda(force=False):

    usda_url = r"https://apps.fas.usda.gov/psdonline/downloads/"

    usda_filenames = [    
        "psd_alldata",
        "psd_eu_area_prod",
    ]

    for filename in usda_filenames:
        download_und_unzip(
            url=usda_url + filename + "_csv.zip",
            savedir=data_dir("raw", "usda"),
            filename=filename + ".csv",
            force=force,
        )

    # DOWNLOAD NEEDED
    # data/raw/usda/{crop}_availability.xls (crop = wheat, rice, maize)
    # https://apps.fas.usda.gov/psdonline/app/index.html#/app/downloads

    # EXTRA DATA SOURCES
    # Crop Calendar Charts & Production Maps
    # https://ipad.fas.usda.gov/ogamaps/cropcalendar.aspx
    # https://ipad.fas.usda.gov/ogamaps/cropproductionmaps.aspx
    # https://ipad.fas.usda.gov/countrysummary/
    # Monthly Crop Stage Calendars
    # https://ipad.fas.usda.gov/ogamaps/cropmapsandcalendars.aspx
    # Field Crops Usual Planting and Harvesting Dates, October 2010 (USA)
    # https://www.nass.usda.gov/Publications/Todays_Reports/reports/fcdate10.pdf
    # Crop Production 2019 Summary, January 2020 (USA)
    # https://www.nass.usda.gov/Publications/Todays_Reports/reports/cropan20.pdf



def download_all_sage(force=False):

    # https://nelson.wisc.edu/sage/data-and-models/crop-calendar-dataset/

    sage_urls = [
        "https://drive.google.com/uc?export=download&id=13vTTrNrnX5V4yLZjqHCkBqZyBMHnzqo7",
        "https://drive.google.com/uc?export=download&id=16qZxhvSDe0YIfqe19XNTA9LJlRO6ZL9D",
        "https://drive.google.com/uc?export=download&id=1riZmvc4y-93pOo7aRHgF4iPqCATIsGjJ",
        "https://drive.google.com/uc?export=download&id=1fuohlT7pCIjA9aatGGlHskBmqnUo4dEU",
    ]

    sage_filenames = [
        "All_data_with_climate.csv",
        "All_data_with_climate.xls",
        "code_lists.txt",
        "crop-readme.txt"
    ]

    for url, filename in zip(sage_urls, sage_filenames):
        download(
            url=url,
            savedir=data_dir("raw", "sage"),
            filename=filename,
            force=force,
        )




if __name__ == "__main__":

    download_all_fao()
    download_all_usda()
    download_all_sage()



