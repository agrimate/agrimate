using OrderedCollections: OrderedDict
import YAML


const AgrimateRegions = """
Argentina: [ARG]
Australia: [AUS]
Canada: [CAN]
China: [CHN, HKG, MAC, TWN]
EU-27: [AUT, BEL, BLX, BGR, CSK, CYP, CZE, DEU, DNK, ESP, EST, FIN, FRA, GRC, HRV, HUN, IRL, ITA, LTU, LUX, LVA, MLT, NLD, POL, PRT, ROU, SVK, SVN, SWE]
India: [IND]
Kazakhstan: [KAZ]
Pakistan: [PAK]
Russia: [RUS]
Turkey: [TUR]
USA: [USA]
Ukraine: [UKR]
Central America: [ABW, AIA, ANT, ATG, BES, BHS, BLM, BLZ, BMU, BRB, CRI, CUB, CUW, CYM, DMA, DOM, GLP, GRD, GTM, HND, HTI, JAM, KNA, LCA, MAF, MEX, MSR, MTQ, NIC, PAN, PRI, SLV, SXM, TCA, TTO, VCT, VGB, VIR]
Rest of Central Asia: [KGZ, TJK, TKM, UZB]
Eastern Africa: [ATF, BDI, COM, DJI, ERI, ETH, IOT, KEN, MDG, MOZ, MUS, MWI, MYT, REU, RWA, SOM, SSD, SYC, TZA, UGA, ZMB, ZWE]
Rest of Eastern Asia: [JPN, KOR, MNG, PRK]
Rest of Europe: [ALA, ALB, AND, BIH, BLR, CHE, FRO, GBR, GGY, GIB, IMN, ISL, JEY, LIE, MCO, MDA, MKD, MNE, NOR, SCG, SJM, SMR, SRB, VAT, YUG, XKX]
Middle Africa: [AGO, CAF, CMR, COD, COG, GAB, GNQ, STP, TCD]
Northern Africa: [DZA, EGY, ESH, LBY, MAR, SDN, TUN]
Rest of Oceania: [ASM, CCK, COK, CXR, FJI, FSM, GUM, HMD, JTN, KIR, MHL, MID, MNP, NCL, NFK, NIU, NRU, NZL, PCN, PLW, PNG, PYF, SLB, TKL, TON, TUV, UMI, VUT, WAK, WLF, WSM]
Rest of South America: [BOL, BRA, BVT, CHL, COL, ECU, FLK, GUF, GUY, PER, PRY, SGS, SUR, URY, VEN]
Rest of Southern Asia: [AFG, BGD, BTN, IRN, LKA, MDV, NPL]
South-Eastern Asia: [BRN, IDN, KHM, LAO, MMR, MYS, PHL, SGP, THA, TLS, VNM]
Southern Africa: [BWA, LSO, NAM, SWZ, ZAF]
Western Africa: [BEN, BFA, CIV, CPV, GHA, GIN, GMB, GNB, LBR, MLI, MRT, NER, NGA, SEN, SHN, SLE, TGO]
Rest of Western Asia: [ARE, ARM, AZE, BHR, GEO, IRQ, ISR, JOR, KWT, LBN, OMN, PSE, QAT, SAU, SYR, YEM, YMD]
"""

const AgrimateEU28Regions = """
Argentina: [ARG]
Australia: [AUS]
Canada: [CAN]
China: [CHN, HKG, MAC, TWN]
EU-28: [AUT, BEL, BLX, BGR, CSK, CYP, CZE, DEU, DNK, ESP, EST, FIN, FRA, GBR, GRC, HRV, HUN, IRL, ITA, LTU, LUX, LVA, MLT, NLD, POL, PRT, ROU, SVK, SVN, SWE]
India: [IND]
Kazakhstan: [KAZ]
Pakistan: [PAK]
Russia: [RUS]
Turkey: [TUR]
USA: [USA]
Ukraine: [UKR]
Central America: [ABW, AIA, ANT, ATG, BES, BHS, BLM, BLZ, BMU, BRB, CRI, CUB, CUW, CYM, DMA, DOM, GLP, GRD, GTM, HND, HTI, JAM, KNA, LCA, MAF, MEX, MSR, MTQ, NIC, PAN, PRI, SLV, SXM, TCA, TTO, VCT, VGB, VIR]
Rest of Central Asia: [KGZ, TJK, TKM, UZB]
Eastern Africa: [ATF, BDI, COM, DJI, ERI, ETH, IOT, KEN, MDG, MOZ, MUS, MWI, MYT, REU, RWA, SOM, SSD, SYC, TZA, UGA, ZMB, ZWE]
Rest of Eastern Asia: [JPN, KOR, MNG, PRK]
Rest of Europe: [ALA, ALB, AND, BIH, BLR, CHE, FRO, GGY, GIB, IMN, ISL, JEY, LIE, MCO, MDA, MKD, MNE, NOR, SCG, SJM, SMR, SRB, VAT, YUG, XKX]
Middle Africa: [AGO, CAF, CMR, COD, COG, GAB, GNQ, STP, TCD]
Northern Africa: [DZA, EGY, ESH, LBY, MAR, SDN, TUN]
Rest of Oceania: [ASM, CCK, COK, CXR, FJI, FSM, GUM, HMD, JTN, KIR, MHL, MID, MNP, NCL, NFK, NIU, NRU, NZL, PCN, PLW, PNG, PYF, SLB, TKL, TON, TUV, UMI, VUT, WAK, WLF, WSM]
Rest of South America: [BOL, BRA, BVT, CHL, COL, ECU, FLK, GUF, GUY, PER, PRY, SGS, SUR, URY, VEN]
Rest of Southern Asia: [AFG, BGD, BTN, IRN, LKA, MDV, NPL]
South-Eastern Asia: [BRN, IDN, KHM, LAO, MMR, MYS, PHL, SGP, THA, TLS, VNM]
Southern Africa: [BWA, LSO, NAM, SWZ, ZAF]
Western Africa: [BEN, BFA, CIV, CPV, GHA, GIN, GMB, GNB, LBR, MLI, MRT, NER, NGA, SEN, SHN, SLE, TGO]
Rest of Western Asia: [ARE, ARM, AZE, BHR, GEO, IRQ, ISR, JOR, KWT, LBN, OMN, PSE, QAT, SAU, SYR, YEM, YMD]
"""

const AgrimateMacroRegions = """
Russia: [RUS]
Central America: [ABW, AIA, ANT, ATG, BES, BHS, BLM, BLZ, BMU, BRB, CRI, CUB, CUW, CYM, DMA, DOM, GLP, GRD, GTM, HND, HTI, JAM, KNA, LCA, MAF, MEX, MSR, MTQ, NIC, PAN, PRI, SLV, SXM, TCA, TTO, VCT, VGB, VIR]
Central Asia: [KAZ, KGZ, TJK, TKM, UZB]
Eastern Africa: [ATF, BDI, COM, DJI, ERI, ETH, IOT, KEN, MDG, MOZ, MUS, MWI, MYT, REU, RWA, SOM, SSD, SYC, TZA, UGA, ZMB, ZWE]
Eastern Asia: [CHN, HKG, MAC, TWN, JPN, KOR, MNG, PRK]
Europe: [ALA, ALB, AND, BIH, BLR, CHE, FRO, GBR, GGY, GIB, IMN, ISL, JEY, LIE, MCO, MDA, MKD, MNE, NOR, SCG, SJM, SMR, SRB, YUG, AUT, BEL, BLX, BGR, CSK, CYP, CZE, DEU, DNK, ESP, EST, FIN, FRA, GRC, HRV, HUN, IRL, ITA, LTU, LUX, LVA, MLT, NLD, POL, PRT, ROU, SVK, SVN, SWE, UKR, VAT, XKX]
Middle Africa: [AGO, CAF, CMR, COD, COG, GAB, GNQ, STP, TCD]
North America: [CAN, USA]
Northern Africa: [DZA, EGY, ESH, LBY, MAR, SDN, TUN]
Oceania: [ASM, AUS, CCK, COK, CXR, FJI, FSM, GUM, HMD, JTN, KIR, MHL, MID, MNP, NCL, NFK, NIU, NRU, NZL, PCN, PLW, PNG, PYF, SLB, TKL, TON, TUV, UMI, VUT, WAK, WLF, WSM]
South America: [ARG, BOL, BRA, BVT, CHL, COL, ECU, FLK, GUF, GUY, PER, PRY, SGS, SUR, URY, VEN]
Southern Asia: [AFG, BGD, BTN, IND, IRN, LKA, MDV, NPL, PAK]
South-Eastern Asia: [BRN, IDN, KHM, LAO, MMR, MYS, PHL, SGP, THA, TLS, VNM]
Southern Africa: [BWA, LSO, NAM, SWZ, ZAF]
Western Africa: [BEN, BFA, CIV, CPV, GHA, GIN, GMB, GNB, LBR, MLI, MRT, NER, NGA, SEN, SHN, SLE, TGO]
Western Asia: [ARE, ARM, AZE, BHR, GEO, IRQ, ISR, JOR, KWT, LBN, OMN, PSE, QAT, SAU, SYR, TUR, YEM, YMD]
"""

const GLOBIOMRegions = """
# GLOBIOM based on
# https://iiasa.github.io/GLOBIOM/GLOBIOM_Documentation_20180604.pdf
# Europe
EU Baltic: [EST, LVA, LTU]
EU Central East: [BGR, CZE, CSK, HUN, POL, ROU, SVK, SVN]
EU Mid West: [AUT, BEL, BLX, DEU, FRA, LUX, NLD]
EU North: [DNK, FIN, IRL, SWE, GBR]
EU South: [CYP, GRC, ITA, MLT, PRT, ESP]
RCEU: [ALB, BIH, HRV, MKD, SCG, SRB, MNE, YUG]
ROWE: [GIB, ISL, NOR, CHE, FRO]
# Former USSR
Russia: [RUS]
Ukraine: [UKR]
Former USSR: [ARM, AZE, BLR, GEO, KAZ, KGZ, MDA, TJK, TKM, UZB]
# Oceania
Australia: [AUS]
New Zealand: [NZL]
Pacific Islands: [COK, FJI, KIR, PNG, WSM, SLB, TON, VUT, NCL, PYF, NIU, NRU, TUV]
# North America
Canada: [CAN]
USA: [USA]
# Latin America
Argentina: [ARG]
Brazil: [BRA]
Mexico: [MEX]
RCAM: [ATG, BHS, BRB, BLZ, BMU, CRI, CUB, DMA, DOM, KNA, SLV, GRD, GTM, HTI, HND, JAM, LCA, NIC, ANT, PAN, ABW, CUW, VCT, SXM, TTO]
RSAM: [BOL, CHL, COL, ECU, GUY, PRY, PER, SUR, URY, VEN]
# Eastern Asia
China: [CHN, TWN, HKG, MAC]
Japan: [JPN]
South Korea: [KOR]
# South-East Asia
Indonesia: [IDN]
Malaysia: [MYS]
RSEA OPA: [BRN, SGP, MMR, PHL, THA, TLS]
RSEA PAC: [KHM, PRK, LAO, MNG, VNM]
# South Asia
India: [IND]
RSAS: [AFG, BGD, BTN, MDV, NPL, PAK, LKA]
# MENA
Middle East: [BHR, IRN, IRQ, ISR, JOR, KWT, LBN, OMN, QAT, SAU, SYR, ARE, YEM, YMD, PSE]
Northern Africa: [DZA, EGY, LBY, MAR, TUN]
Turkey: [TUR]
# Sub-saharan Africa
Congo Basin: [CMR, CAF, COG, COD, GNQ, GAB, STP]
Eastern Africa: [BDI, ETH, KEN, RWA, TZA, UGA]
South Africa: [ZAF]
Southern Africa: [AGO, BWA, COM, LSO, MDG, MWI, MUS, MOZ, NAM, SWZ, ZMB, ZWE, SYC]
West and Central Africa: [BEN, BFA, CPV, TCD, CIV, DJI, ERI, GMB, GHA, GIN, GNB, LBR, MLI, MRT, NER, NGA, SEN, SLE, SOM, SDN, SSD, TGO]
"""

const MAgPIERegions = """
# MAgPIE based on
# https://gmd.copernicus.org/articles/12/1299/2019/gmd-12-1299-2019.pdf
# https://github.com/magpiemodel/magpie/blob/master/core/sets.gms
CAZ: [AUS, CAN, HMD, NZL, SPM]
CHA: [CHN, HKG, MAC, TWN]
EUR: [ALA, AUT, BEL, BLX, BGR, CYP, CSK, CZE, DEU, DNK, ESP, EST, FIN, FRA, FRO, GBR, GGY, GIB, GRC, HRV, HUN, IMN, IRL, ITA, JEY, LTU, LUX, LVA, MLT, NLD, POL, PRT, ROU, SCG, SVK, SVN, SWE, YUG]
IND: [IND]
JPN: [JPN]
LAM: [ABW, AIA, ARG, ATA, ATG, BES, BHS, BLM, BLZ, BMU, BOL, BRA, BRB, BVT, CHL, COL, CRI, CUB, CUW, CYM, DMA, DOM, ECU, FLK, GLP, GRD, GTM, GUF, GUY, HND, HTI, JAM, KNA, LCA, MAF, MEX, MSR, MTQ, NIC, PAN, PER, PRI, PRY, SGS, SLV, SUR, SXM, TCA, TTO, URY, VCT, VEN, VGB, VIR]
MEA: [ARE, BHR, DZA, EGY, ESH, IRN, IRQ, ISR, JOR, KWT, LBN, LBY, MAR, OMN, PSE, QAT, SAU, SDN, SYR, TUN, YEM, YMD]
NEU: [ALB, AND, BIH, CHE, GRL, ISL, LIE, MCO, MKD, MNE, NOR, SJM, SMR, SRB, TUR, VAT]
OAS: [AFG, ASM, ATF, BGD, BRN, BTN, CCK, COK, CXR, FJI, FSM, GUM, IDN, IOT, KHM, KIR, KOR, LAO, LKA, MDV, MHL, MMR, MNG, MNP, MYS, NCL, NFK, NIU, NPL, NRU, PAK, PCN, PHL, PLW, PNG, PRK, PYF, SGP, SLB, THA, TKL, TLS, TON, TUV, UMI, VNM, VUT, WLF, WSM]
REF: [ARM, AZE, BLR, GEO, KAZ, KGZ, MDA, RUS, TJK, TKM, UKR, UZB]
SSA: [AGO, BDI, BEN, BFA, BWA, CAF, CIV, CMR, COD, COG, COM, CPV, DJI, ERI, ETH, GAB, GHA, GIN, GMB, GNB, GNQ, KEN, LBR, LSO, MDG, MLI, MOZ, MRT, MUS, MWI, MYT, NAM, NER, NGA, REU, RWA, SEN, SHN, SLE, SOM, SSD, STP, SWZ, SYC, TCD, TGO, TZA, UGA, ZAF, ZMB, ZWE]
USA: [USA]
"""

const agrimate_regions = Dict(
    "Agrimate" => AgrimateRegions,
    "AgrimateEU28" => AgrimateEU28Regions,
    "AgrimateMacro" => AgrimateMacroRegions,
    "GLOBIOM" => GLOBIOMRegions,
    "MAgPIE" => MAgPIERegions,
)


parse_regions(regions::String) = YAML.load(regions)


function sort_names(regions::AbstractDict)
    for areas in values(regions)
        sort!(areas)
    end
    regions = OrderedDict(name => regions[name] for name in sort(collect(keys(regions))))
    return regions
end


function check_validity(regions::AbstractDict)
    all = String[]
    for areas in values(regions)
        all = vcat(all, areas)
    end
    println("Number of all areas: $(length(all))")
    println("Number of unique areas: $(length(unique(all)))")
end


function save_as_yaml(file, dict)
    open(file, "w") do io
        show(io, "text/plain", dict)
    end
    lines = readlines(file)[2:end]
    yaml_lines = String[]
    for line in lines
        line = lstrip(line)
        line = replace(line, "\"" => "")
        line = replace(line, " =>" => ":")
        push!(yaml_lines, line)
    end
    yaml_string = join(yaml_lines, "\n") * "\n"
    open(file, "w") do io
        write(io, yaml_string)
    end
    return yaml_string
end


get_regions(regions::String) = agrimate_regions[regions] |> parse_regions |> sort_names


# save_as_yaml("regions.yaml", get_regions("Agrimate"))