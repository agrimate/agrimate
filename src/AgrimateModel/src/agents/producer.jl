module ProducerModule

export harvest_step!, sales_step!, communication_step!, get_storage_timeseries

using Parameters
using LinearAlgebra: LowerTriangular
using Statistics: mean

using ..AgentsModule

# Producers' actions

function harvest_step!(producer, harvest::Float64, expected_harvests::Vector{Float64})
    @unpack δ = producer.local_params
    producer.harvest = harvest
    producer.expected_harvests[:] .= expected_harvests
    producer.storage *= 1 - δ
end

function sales_step!(producer, t, consumers::Dict{String,Consumer}; verbose::Bool = true)

    @unpack N_year, N_hor, ι = producer.global_params
    @unpack X_star = producer.global_params
    @unpack α, β, λ, δ, ρ, p_sto, τ_P, σ = producer.local_params
    @unpack X_avg = producer.local_params
    @unpack market_constraint, unique_market_constraint, rationing = producer.local_params

    # Update local price adjustment factor
    D_tot = reduce(+, d.quantity for d in producer.demand_requests; init = 0.)
    X̂ = producer.expected_sales
    P_loc = producer.price_adjustment_factor
    P_loc_tgt = determine_target_price_adjustment_factor(P_loc, D_tot, X̂, β, ι * X_avg)
    if producer.baseline_sales[mod(t, 1:N_year)] ≤ ι * X_avg
        P_loc_tgt = 1.
    end
    producer.price_adjustment_factor = 1 / τ_P * P_loc_tgt + (1 - 1 / τ_P) * P_loc

    # Sort demand requests
    sort!(producer.demand_requests, lt = (d1, d2) -> d1.price > d2.price)

    # Form current revenue expectation
    (revenue_curve, thresholds) = determine_revenue_curve(producer.demand_requests; rationing)

    # All the demand requests are satisfied if the availability permits
    X1 = min(D_tot, producer.harvest + producer.storage) 

    # Optimize sales
    H = vcat(producer.harvest, producer.expected_harvests)
    S_beg = producer.storage
    S_end = producer.baseline_storage[mod(t + N_hor, 1:N_year)]
    X_oth = producer.expected_others_sales
    X_init = vcat(
        producer.optimal_sales[2:end],
        producer.baseline_sales[mod(t + N_hor, 1:N_year)]
    )
    verbose && println("  Optimizing sales of $(producer.id)...")
    producer.optimal_sales[:] =
        X_avg .* optimize_sales(;
            revenue_curve = (x1) -> revenue_curve(x1 * X_avg) ./ (X_avg, 1),
            x1_max = X1 / X_avg,
            x1_min = X1 / X_avg,
            x1_thresholds = thresholds ./ X_avg,
            h = H ./ X_avg,
            s_beg = S_beg / X_avg,
            s_end = S_end / X_avg,
            x_oth = X_oth ./ X_avg,
            x_init = X_init ./ X_avg,
            P_loc = producer.price_adjustment_factor,
            α,
            λ,
            δ,
            ρ,
            p_sto,
            x_star = X_star / X_avg,
            N_hor,
            x_market = (market_constraint ? producer.market_size / X_avg : nothing),
            x_market_unique = (unique_market_constraint ? producer.market_size_unique / X_avg : nothing),
            tol = 1e-8 / X_avg,
            verbose = false,
        )

    # Update transactions
    producer.total_sales = get_current_sales(producer.optimal_sales)
    empty!(producer.transactions)
    producer.transactions =
        determine_transactions(producer.total_sales, producer.demand_requests; rationing)

    # Send transactions
    for transaction in producer.transactions
        customer = consumers[transaction.buyer_id]
        push!(customer.transactions, transaction)
    end

    # Update storage
    producer.storage += producer.harvest - producer.total_sales
    @assert producer.storage > -1e-4
    producer.storage = max(0, producer.storage)
end

function communication_step!(producer, t, producers::Dict{String,Producer}, export_restriction = 0)
    @unpack N_year, N_hor = producer.global_params
    @unpack X_star = producer.global_params
    @unpack α, λ, τ_exp, σ = producer.local_params

    # Set export restriction and export tax
    producer.export_restriction = export_restriction
    producer.export_tax_factor = (1 - producer.export_restriction)^(-1/σ) 

    # Communicate expected sales quantity and price
    producer.expected_sales = get_expected_sales(producer.optimal_sales)
    producer.expected_price =
        P(
            (producer.expected_sales + first(producer.expected_others_sales)) /
            X_star,
            α,
            λ,
        ) * producer.price_adjustment_factor
    # Form expectation on other producers' sales
    expected_others_sales = sum(
        get_expected_future_sales(
            other_producer.optimal_sales,
            other_producer.baseline_sales,
            t,
            N_hor,
            N_year,
        ) for other_producer in values(producers) if other_producer.id != producer.id
    )
    producer.expected_others_sales[1:N_hor-1] =
        1 / τ_exp .* expected_others_sales[1:N_hor-1] +
        (1 - 1 / τ_exp) .* producer.expected_others_sales[2:N_hor]
    producer.expected_others_sales[end] = expected_others_sales[end]
    # Prepare for new demand requests
    empty!(producer.demand_requests)
end

# Auxiliary functions

function P(x, α, λ)
    if λ == 0
        return x .^ (-α)
    elseif λ == 1
        return 1 .- α .* (x .- 1)
    else
        return (1 .- α .* (x .^ λ .- 1)) .^ (1 / λ)
    end
end

function P_inv(p, α, λ)
    if λ == 0
        return p.^(-1 / α)
    elseif λ == 1
        return 1 .- (p .- 1) ./ α
    else
        return (1 .- (p.^λ .- 1) ./ α).^(1 / λ)
    end
end

function grad_P(x, α, λ)
    if λ == 0
        return -α .* x .^ (-α - 1)
    elseif λ == 1
        return -α .* ones(length(x))
    else
        #return -α .* x.^(λ .- 1) .* (α .- α .* x.^λ .+ 1).^(1/λ - 1)
        return -α .* (P(x, α, λ) ./ x) .^ (1 - λ)
    end
end

function determine_J(total_sales, sorted_demand_requests)
    if total_sales == 0
        return Int[1]
    end
    @assert total_sales ≤ sum(d.quantity for d in sorted_demand_requests)
    # Find one possible J
    J = Int[]
    let X = total_sales
        for j = 1:lastindex(sorted_demand_requests)
            X -= sorted_demand_requests[j].quantity
            if X ≤ 0 || j == lastindex(sorted_demand_requests)
                push!(J, j)
                break
            end
        end
    end
    # Find all other possible J's
    while first(J) > firstindex(sorted_demand_requests)
        if sorted_demand_requests[first(J)-1].price ==
           sorted_demand_requests[first(J)].price
            pushfirst!(J, first(J) - 1)
        else
            break
        end
    end
    while last(J) < lastindex(sorted_demand_requests)
        if sorted_demand_requests[last(J)+1].price == sorted_demand_requests[last(J)].price
            push!(J, last(J) + 1)
        else
            break
        end
    end
    return J
end


function determine_transactions(total_sales, sorted_demand_requests; rationing = :price)
    if !isempty(sorted_demand_requests)
        total_demand = sum(d.quantity for d in sorted_demand_requests)
    else
        return Transaction[]
    end
    if total_sales > total_demand && total_sales - total_demand < 1e-5
        total_sales = total_demand
    end
    @assert total_sales ≤ total_demand
    transactions = Transaction[]
    if rationing == :price
        J = determine_J(total_sales, sorted_demand_requests)
        append!(transactions, sorted_demand_requests[1:first(J)-1])
        remaining_sales = total_sales - reduce(+, t.quantity for t in transactions; init = 0.0)
        remaining_demand = sum(d.quantity for d in sorted_demand_requests[J])
        if remaining_sales > 0
            ratio = remaining_sales / remaining_demand
            for d in sorted_demand_requests[J]
                transaction = Transaction(d.seller_id, d.buyer_id, d.quantity * ratio, d.price)
                push!(transactions, transaction)
            end
        end
    elseif rationing == :proportional
        if total_demand > 0
            ratio = total_sales / total_demand
            for d in sorted_demand_requests
                transaction = Transaction(d.seller_id, d.buyer_id, d.quantity * ratio, d.price)
                push!(transactions, transaction)
            end
        end
    end
    return transactions
end

function get_current_sales(optimal_sales)
    return optimal_sales[1]
end

function get_expected_sales(optimal_sales)
    return optimal_sales[2]
end

function get_expected_future_sales(optimal_sales, baseline_sales, t, N_hor, N_year)
    return vcat(optimal_sales[3:end], baseline_sales[mod(t + 1 + N_hor, 1:N_year)])
end

function determine_target_price_adjustment_factor(P, D_tot, X̂, β, min_X̂)
    if X̂ < min_X̂
        return 1
    end
    return P * (D_tot / X̂)^β
end

function determine_revenue_curve(sorted_demand_requests; rationing = :price)
    if rationing == :price
        X_thresholds = Float64[]
        constants = Float64[]
        prices = Float64[]
        if !isempty(sorted_demand_requests)
            let X = 0, C = 0
                for demand_request in sorted_demand_requests
                    push!(prices, demand_request.price)
                    push!(constants, C - demand_request.price * X)
                    X += demand_request.quantity
                    C += demand_request.price * demand_request.quantity
                    push!(X_thresholds, X)
                end
            end
        else
            push!(X_thresholds, 0)
            push!(prices, 0)
            push!(constants, 0)
        end
        revenue_curve = function(X; max_slope = Inf)
            if X > last(X_thresholds) && X - last(X_thresholds) ≤ 1e-5
                X = last(X_thresholds)
            end
            # @assert X ≤ last(X_thresholds)
            if X ≤ last(X_thresholds)
                j = searchsortedfirst(X_thresholds, X)
            else
                return (constants[end] + prices[end] * X_thresholds[end], 0.)
            end
            value = constants[j] + prices[j] * X
            # Regularized derivative
            if max_slope == Inf
                der = prices[j]
            else
                # Replace the step by a line with slope max_slope
                ΔX_left = (j > 1 ? (prices[j-1] - prices[j]) / (2 * max_slope) : 0)
                ΔX_right =
                    (j < length(X_thresholds) ? (prices[j] - prices[j+1]) / (2 * max_slope) : 0)

                left_to_X = X - (j > 1 ? X_thresholds[j-1] : 0)
                X_to_right = X_thresholds[j] - X

                @assert ΔX_left + ΔX_right ≤ left_to_X + X_to_right

                if left_to_X < ΔX_left
                    der = prices[j] + max_slope * (ΔX_left - left_to_X)
                elseif X_to_right < ΔX_right
                    der = prices[j] - max_slope * (ΔX_right - X_to_right)
                else
                    der = prices[j]
                end
            end
            return (value, der)
        end
        return (revenue_curve, X_thresholds)
    elseif rationing == :proportional
        average_price = 0
        total_demand = 0
        for demand_request in sorted_demand_requests
            average_price += demand_request.price * demand_request.quantity
            total_demand += demand_request.quantity
        end
        if total_demand > 0
            average_price /= total_demand
        end
        X_thresholds = [total_demand]
        revenue_curve = function(X; max_slope = Inf)
            if X ≤ total_demand
                return (X * average_price, average_price)
            else
                return (total_demand * average_price, 0)
            end
        end
        return (revenue_curve, X_thresholds)
    end
    @assert rationing in [:price, :proportional]
end

function get_storage_timeseries(H::AbstractVector, X::AbstractVector, S_beg; δ = 0)
    # S_beg = (1 - δ) * s_init
    @assert length(H) == length(X)
    N = length(H)
    L = LowerTriangular(ones((N, N)))
    for index in CartesianIndices(L)
        L[index] *= (1 - δ)^(index[1] - index[2])
    end
    S = L * (H - X) + L[:, begin] .* S_beg
    @assert minimum(S) > -1e-4
    S[S.<0] .= 0
    return S
end

function get_L_matrix(N; δ = 0)
    L = Matrix{Float64}(undef, N, N)
    for index in CartesianIndices(L)
        if index[1] ≥ index[2]
            L[index] = (1 - δ)^(index[1] - index[2])
        end
    end
    return LowerTriangular(L)
end

function get_unit_storage_costs(N, p_sto; δ = 0, ρ = 0)
    γ = (1 - δ) / (1 + ρ)
    if γ == 1.0
        unit_costs = -p_sto .* [N - (n - 1.0) for n = 1:N]
    else
        unit_costs = -p_sto .* [1.0 - γ^(N - (n - 1.0)) / (1 - γ) for n = 1:N]
    end
    return unit_costs
end

function profit_function(x, x_oth, x_star, α, λ, unit_costs, h; P_loc = 1, ρ = 0)
    revenues = (P((x + x_oth) ./ x_star, α, λ) .* P_loc) .* x
    costs = unit_costs .* (x - h)
    N = length(x)
    return sum((revenues - costs) ./ (1 + ρ).^(0:N-1))
end

function grad_profit_function(x, x_oth, x_star, α, λ, unit_costs; P_loc = 1, ρ = 0)
    grad =
        P((x + x_oth) ./ x_star, α, λ) .* P_loc +
        grad_P((x + x_oth) ./ x_star, α, λ) .* P_loc .* (x ./ x_star) - unit_costs
    N = length(x)
    return grad ./ (1 + ρ) .^ (0:N-1)
end

function quadratic_penalty(x, x_avg::Union{Float64,Nothing} = nothing; ρ = 0.)
    if isnothing(x_avg)
        # Treat x_avg as a function of x
        x_avg = mean(x)
    end
    N = length(x)
    return sum((x .- x_avg).^2 ./ (1 + ρ).^(0:N-1)) / x_avg
end

function grad_quadratic_penalty(x, x_avg::Union{Float64,Nothing} = nothing; ρ = 0.)
    extra_gradient_term = false
    if isnothing(x_avg)
        # Treat x_avg as a function of x
        x_avg = mean(x)
        extra_gradient_term = true
    end
    N = length(x)
    grad = 2 .* (x .- x_avg) ./ x_avg ./ (1 + ρ).^(0:N-1)
    if extra_gradient_term
        # Extra term if x_avg is a function of x
        grad[:] .-= sum((x .- x_avg) .* (x .+ x_avg) ./ (1 + ρ).^(0:N-1)) / x_avg^2 / N
    end
    return grad
end

include("producer_optimization.jl")

end
