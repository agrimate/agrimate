module AgrimateModel

export AgrimateParams, InitializationData, InputData
export @AgrimateParamsMix
export initialize_model, initialize_model_run, run_model!

using DataFrames: DataFrame, allowmissing!
using Mixers
using Parameters

include("agents.jl")
include("model.jl")
include("run.jl")

using .AgentsModule, .ModelModule, .RunModule


@mix @with_kw struct AgrimateParamsMix
    N_year::Int = 24                                # timesteps per year
    N_hor::Int = 2 * N_year                         # timesteps of producer optimization horizon
    N_del::Int = 1 * (N_year ÷ 12)                  # timesteps per delivery
    α::Float64 = 5.0                                # - inverse elasticity of demand assumed by producers
    λ::Float64 = 0.0                                # λ=1 => linear / λ=0 => power law inverse demand function assumed by producer
    δ::Float64 = 0.0                                # storage deterioration (per year)
    ρ::Float64 = 0.0                                # interest rate (per year)
    β::Float64 = 0.05                               # strength of producers' local price adjustment
    p_sto::Float64 = 0.1                            # unit storage cost (per year)
    τ_exp::Float64 = 0.2                            # timescale (in yearly unit) of updating expectations on other producers' sales
    τ_P::Float64 = 0.2                              # timescale (in yearly unit) of local price adjustment
    σ::Float64 = 2.                                 # supplier substitution elasticity
    τ::Float64 = 0.2                                # timescale (in yearly unit) of balancing consumer inventory
    ψ::Union{Float64,Symbol} = 0.3                  # target stock-to-use ratio (annual)
    ε_d::Float64 = 1 / α                            # - price elasticity of demand
    ε_c::Float64 = 0.1                              # - price elasticity of consumption
    τ_a::Float64 = 0.2                              # timescale (in yearly unit) of supplier change due to export restrictions
    A_d_star::Union{Float64,Symbol} = 0.2           # baseline share of purchaser budget spent on commodity
    A_c_star::Union{Float64,Symbol} = 0.2           # baseline share of household budget spent on commodity
    market_constraint::Bool = true                  # apply max constraint in producer optimization?
    unique_market_constraint::Bool = true           # apply min constraint in producer optimization?
    rationing::Symbol = :proportional               # rationing scheme (all reservation prices are equal so this paramater doesn't matter) 
    N_for::Int = 3 * (N_year ÷ 12)                  # timesteps for which accurate harvest forecast is possbile
    τ_for::Float64 = 0.2                            # timescale (in yearly units) of transition between accurate and baseline forecast
    ι::Float64 = 0.001                              # relative cutoff under which no demand requests and price adjustment are made (for stability reasons)
end

@AgrimateParamsMix @with_kw struct AgrimateParams{} end


function initialize_model(
    init_data::InitializationData,
    agrimate_params::AgrimateParams;
    n_start = 1,
    κ = 0.25,
    tol = 1e-8,
    verbose = true,
    empirical_params::Dict = Dict(),
)

    @assert n_start == 1 "Start date must be 1st January"
    # TODO: replace t by a tuple (t, n_t); otherwise results with n_start != 1 will be incorrect

    @unpack δ = agrimate_params
    @assert δ == 0 "Deterioration must be 0"
    # TODO: initialization should capture deterioration correctly (e.g. total harvest should exceed total consumption)
    # TODO: unique market constraint does not take deterioration correctly into account

    # Global params    
    @unpack N_year, N_hor, N_del, ι = agrimate_params
    global_params = (; N_year, N_hor, N_del, ι)

    # Producer params
    @unpack α, λ, β, σ, δ, ρ, p_sto, τ_exp, τ_P, N_for, τ_for = agrimate_params
    @unpack market_constraint, unique_market_constraint, rationing = agrimate_params
    producer_params = (; 
        α, λ, β, δ, ρ, σ,
        N_for = N_for,
        p_sto = p_sto / N_year, 
        τ_exp = τ_exp * N_year, τ_P = τ_P * N_year, τ_for = τ_for * N_year,
        market_constraint, unique_market_constraint, rationing,
    )
    @assert producer_params.τ_exp ≥ 1.
    @assert producer_params.τ_P ≥ 1.

    # Consumer params
    @unpack α, σ, δ, τ, ψ, ε_c, ε_d, τ_a, A_d_star, A_c_star = agrimate_params
    consumer_params = (;
        α, σ, δ, ε_d, ε_c, A_d_star, A_c_star,
        ψ = (ψ isa Symbol ? ψ : ψ * N_year),
        τ = τ * N_year, τ_a = τ_a * N_year,
    )
    @assert consumer_params.τ ≥ 1.
    @assert consumer_params.τ_a ≥ 1.

    # Producers and consumer ids
    producer_ids = collect(keys(init_data.baseline_harvests))
    consumer_ids = collect(keys(init_data.baseline_consumption))

    model = Model(;global_params)
    create_agents!(model; producer_ids, consumer_ids, producer_params, consumer_params)

    # Load empirical params if present
    for (param, value) in Dict(pairs((;ψ, A_d_star, A_c_star)))
        !(value isa Symbol) && continue
        for (id, consumer) in model.consumers
            consumer.local_params[param] = empirical_params[param][id]
            if param == :ψ
                consumer.local_params[param] *= N_year
            end
        end
    end

    output_baseline_data = DataFrame(
        t = Int[],
        n = Int[],
        producer = String[],
        consumer = String[],
        variable = String[],
        value = Float64[],
    )
    allowmissing!(output_baseline_data)

    for (param, values) in empirical_params
        for (consumer_id, value) in values
            push!(
                output_baseline_data,
                Dict(
                    :t => missing,
                    :n => missing,
                    :producer => missing,
                    :consumer => consumer_id,
                    :variable => "$param",
                    :value => value,
                ),
            )
        end
    end

    initialize_agents!(model, init_data; n_start, output_baseline_data, κ, tol, verbose)
    return (model, output_baseline_data)
end





end
