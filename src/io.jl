using CodecZlib
using CSV: CSV
using DataFrames: DataFrame
using FileIO: FileIO
using JSON: JSON

macro source()
    return QuoteNode(__source__)
end

function DrWatson._wsave(file, df::DataFrame; meta = nothing)
    args = !endswith(file, ".gz") ? (file, "w") : (GzipCompressorStream, file, "w")
    open(args...) do stream
        if !isnothing(meta)
            write(stream, "#meta " * JSON.json(meta) * "\n")
            CSV.write(stream, df; append = true, writeheader = true)
        else
            CSV.write(stream, df)
        end
    end
end

function DrWatson._wsave(file, data::NamedTuple{(:df, :meta),Tuple{DataFrame,T}}) where {T}
    return DrWatson._wsave(file, data.df; meta = data.meta)
end

function DrWatson.wload(file; metaonly = false)
    if endswith(file, ".csv") || endswith(file, ".csv.gz")
        args = !endswith(file, ".gz") ? (file, "r") : (GzipDecompressorStream, file, "r")
        open(args...) do stream
            firstline = first(eachline(stream))
            if !metaonly
                seekstart(stream)
                df = CSV.read(stream, DataFrame; comment = "#")
            end
            if startswith(firstline, "#meta ")
                meta = JSON.parse(firstline[7:end])
                return metaonly ? meta : (; df, meta)
            elseif !startswith(firstline, "#meta ") && metaonly
                return nothing
            else
                return df
            end
        end
    else
        return FileIO.load(file)
    end
end



