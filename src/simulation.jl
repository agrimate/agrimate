using DrWatson
@quickactivate "Agrimate"

using Dates
using Parameters
using Statistics: median

using AgrimateModel


include(srcdir("params.jl"))
include(srcdir("regions.jl"))
include(srcdir("io.jl"))
include(srcdir("preprocess.jl"))


function simulate(params::Params = Params(); 
    t_max = 0, 
    N_save = 24,
    verbose::Bool = true, 
    inputroot = get(ENV, "AGRIMATE_INPUT_ROOT", datadir()), 
    outputroot = get(ENV, "AGRIMATE_OUTPUT_ROOT", datadir()),
    compress_output = false,
    use_cache = false, 
    source = @source,
    processid = nothing,
)
    timestamp_begin = now()

    meta = Dict(:params=>struct2dict(params), :simulation_begin=>timestamp_begin)
    tag!(meta; source)

    @unpack start, N_year = params

    @info "$timestamp_begin | AGRIMATE SIMULATION" source t_max params
        
    inputdir(path...) = joinpath(inputroot, "agrimate_input", path...)
    outputdir(path...) = joinpath(outputroot, "agrimate_output", path...)
    cachedir(path...) = joinpath(
        outputroot, 
        "agrimate_cache", 
        (!isnothing(processid) ? ("$processid",) : ())..., 
        path... 
    )

    @info "$(now()) | INITIALIZATION"
    flush(stderr)

    @unpack crops, baseline = params
    @unpack regions, extra_regions, flow_cutoff, production_cutoff = params

    crop = split(crops, ",")[1] |> string

    df_region = make_regions_dataframe(get_regions(regions), extra_regions)

    filename = savename("food_balance", (;baseline, crop), "csv"; savename_kwargs...)
    df_baseline_food_balance_non_agg = wload(inputdir(filename))

    filename = savename("trade_flows", (;baseline, crop), "csv"; savename_kwargs...)
    df_baseline_trade_flows_non_agg = wload(inputdir(filename))

    filename = savename("harvest_distributions", (;crop), "csv"; savename_kwargs...)
    df_harvest_distributions = wload(inputdir(filename))

    df_harvest_distributions = aggregate_harvest_distributions(
        df_harvest_distributions,
        df_region;
        df_baseline_production=df_baseline_food_balance_non_agg[:, [:Area, :Production]],
    )

    harvest_distributions = Dict(
        String(row[:Area]) => [val for val in row[Between("1", "365")]] 
        for row in eachrow(df_harvest_distributions)
    )

    df_baseline_food_balance = aggregate_areas(df_baseline_food_balance_non_agg, df_region)
    df_baseline_trade_flows = aggregate_areas(df_baseline_trade_flows_non_agg, df_region)
    df_baseline_trade_flows = infer_trade_flows(df_baseline_trade_flows, df_baseline_food_balance)

    baseline_trade_flows = Dict(
        (String(row[:Origin]), String(row[:Destination])) => row["Trade Flow"] / N_year
        for row in eachrow(df_baseline_trade_flows)
    )

    baseline_transactions = apply_cutoffs_to_trade_network(baseline_trade_flows; flow_cutoff, production_cutoff)
    (baseline_production, baseline_consumption) = get_baseline_production_and_consumption(baseline_transactions)
    baseline_harvests = generate_baseline_harvests(baseline_production, harvest_distributions; N_year)

    init_data = InitializationData(
        baseline_harvests,
        baseline_consumption,
        baseline_transactions,
    )

    @unpack ψ, A_d_star, A_c_star = params
    param_sources = unique(split(string(v), "_")[begin] for v in [ψ, A_d_star, A_c_star] if v isa Symbol)
    @assert length(param_sources) < 2
    if length(param_sources) == 1
        source = string(param_sources[1])
        filename = savename("parameters", (;baseline, crop, source), "csv"; savename_kwargs...)
        df_empirical_params = wload(inputdir(filename))
        # Update empirical params if a special value for a selected area is found in the suffix
        for (col_name, value) in [
            "STU" => ψ, 
            "A_d" => A_d_star, 
            "A_c" => A_c_star,
        ]
            value = split(string(value), "_")
            length(value) == 1 && continue
            (area, value) = split(value[end], "=")
            df_empirical_params[findfirst(==(area), df_empirical_params.Area), col_name] = parse(Float64, value)
        end
        df_consumption = df_baseline_food_balance_non_agg[!, [:Area, :Consumption]] 
        empirical_params = generate_empirical_params(params, df_empirical_params, df_consumption, df_region)
    elseif length(param_sources) == 0
        empirical_params = Dict()
    end

    init_dict, path = produce_or_load(
        cachedir(), 
        params; 
        force =! use_cache,
        ignores = [:production_anomalies, :export_restrictions, :start],
        savename_kwargs...
    ) do params
        flush(stderr)

        agrimate_params = AgrimateParams(;(field => getfield(params, field) for field in fieldnames(AgrimateParams))...)

        n_start = calculate_year_and_n(start; N_year)[2]

        (model, output_baseline_data) = initialize_model(
            init_data,
            agrimate_params;
            n_start,
            κ = 0.2,
            tol = 1e-4,
            verbose,
            empirical_params,
        )

        return Dict("model" => model, "output_baseline_data" => output_baseline_data)

    end

    @unpack model, output_baseline_data = init_dict

    add_datetime_col!(output_baseline_data; start, N_year, timestep_field = :n)
    output_baseline_data[!, :value] = round.(output_baseline_data[!, :value]; digits=4)
    output_filename = outputdir(savename(params, !compress_output ? "csv" : "csv.gz"; savename_kwargs...))
    
    meta[:simulation_end] = now()
    wsave(output_filename, (;df=output_baseline_data, meta))
    @info "$(now()) | Saved baseline output: $output_filename"

    if t_max == 0
        return
    end


    @info "$(now()) | RUN"
    flush(stderr)

    @unpack N_hor = model.global_params

    @unpack production_anomalies = params

    if isa(production_anomalies, String)
        filename = savename("harvest_anomalies", (;source=production_anomalies, crop), "csv"; savename_kwargs...)
        df_harvest_anomalies = wload(inputdir(filename))
        df_harvest_anomalies = aggregate_areas(df_harvest_anomalies, df_region)

        filename = savename("harvest_trends", (;source=production_anomalies, crop), "csv"; savename_kwargs...)
        df_harvest_trends = wload(inputdir(filename)) 
        df_harvest_trends = aggregate_areas(df_harvest_trends, df_region)

        date_start = timestep_to_datetime(0.5; start, N_year) |> Date
        date_end = timestep_to_datetime(t_max + N_hor + 0.5; start, N_year) |> Date

        (year_start, day_start) = calculate_year_and_n(date_start; N_year=365)
        (year_end, day_end) = calculate_year_and_n(date_end; N_year=365)

        @assert "$year_start-$day_start" in names(df_harvest_trends)
        if "$year_end-$day_end" in names(df_harvest_trends)
            columns = Between("$year_start-$day_start", "$year_end-$day_end")
            missing_days = 0
        else
            (last_year, last_day) = parse.(Int, split(names(df_harvest_trends)[end], "-"))
            columns = Between("$year_start-$day_start", "$last_year-$last_day")
            missing_days = 365 * (year_end - last_year) + (day_end - last_day)
        end

        harvest_anomalies = Dict(
            String(row[:Area]) => [val for val in row[columns]] 
            for row in eachrow(df_harvest_anomalies)
        )
        harvest_trends = Dict(
            String(row[:Area]) => [val for val in row[columns]] 
            for row in eachrow(df_harvest_trends)
        )

        harvest_forcings = Dict(
            area => vcat(
                replace(1 .+ harvest_anomalies[area] ./ harvest_trends[area], NaN => 1.),
                ones(missing_days),
            ) 
            for area in keys(harvest_trends)
        )
        y_timeseries = date_to_fractional_year(date_start):(1/365):date_to_fractional_year(date_end) |> collect
    else
        harvest_forcings = nothing
        y_timeseries = nothing
    end

    harvests = generate_harvests(
        production_anomalies, 
        baseline_production, 
        baseline_harvests, 
        harvest_distributions,
        harvest_forcings,
        y_timeseries;
        start,
        t_max, 
        N_year, 
        N_hor,
    )

    @unpack export_restrictions = params

    if isa(export_restrictions, String)
        filename = savename("export_restrictions", (;source=export_restrictions, crop), "csv"; savename_kwargs...)
        df_export_restrictions = wload(inputdir(filename))
        df_export_restrictions = aggregate_export_restrictions(
            df_export_restrictions,
            df_region, 
            df_baseline_trade_flows_non_agg, 
            df_baseline_trade_flows
        )
        export_restrictions = [
            String(row[:Exporter]) => Dict(
                :from => row[:From],
                :to => row[:To],
                :value => row[:Value],
            )
            for row in eachrow(df_export_restrictions)
        ]
    end

    export_restriction_intervals = generate_export_restriction_intervals(export_restrictions; start, N_year)

    input_data = InputData(
        harvests,
        export_restriction_intervals,
    )

    run = initialize_model_run(model, input_data, init_data)

    N_chunk = ceil(Int, t_max / N_save)

    for chunk = 1:N_chunk
        t_start = (chunk - 1) * N_save + 1
        t_end = min(chunk * N_save, t_max)
        timesteps = t_end - t_start + 1

        @info "$(now()) | Timesteps t ∈ [$t_start, $t_end] (t_max=$t_max)"
        flush(stderr)

        run_model!(run, timesteps; verbose)

        output_data = copy(run.output_data)
        add_datetime_col!(output_data; start, N_year)
        output_data[!, :value] = round.(output_data[!, :value]; digits=4)
        combined_output_data = vcat(output_baseline_data, output_data)

        meta[:simulation_end] = now()
        wsave(output_filename, (;df=combined_output_data, meta))
        @info "$(now()) | Saved output: $output_filename"
        flush(stderr)
    end
end

